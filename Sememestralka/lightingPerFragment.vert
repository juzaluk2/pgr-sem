#version 140

struct Material {
  vec3  ambient;
  vec3  diffuse;
  vec3  specular;
  float shininess;
  bool  useTexture;
};

uniform mat4 PVMmatrix;
uniform mat4 Vmatrix;
uniform mat4 Mmatrix;
uniform mat4 normalMatrix;
uniform Material material;

in vec3 position;
in vec3 normal;
in vec2 texCoord;

out vec3 fragPosition;
out vec3 fragNormal;
out vec2 fragTexCoord;

void main() {
  // Transform vertex position to eye coordinates
  fragPosition = (Vmatrix * Mmatrix * vec4(position, 1.0)).xyz;
  // Transform normal to eye coordinates
  fragNormal = normalize((normalMatrix * vec4(normal, 0.0)).xyz);
  // Pass texture coordinates to fragment shader
  fragTexCoord = texCoord;
  
  // Project the vertex position to clip space
  gl_Position = PVMmatrix * vec4(position, 1.0);
}