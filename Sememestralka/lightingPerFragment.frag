#version 140

struct Material {
  vec3  ambient;
  vec3  diffuse;
  vec3  specular;
  float shininess;
  bool  useTexture;
};

struct Light {
    vec3  ambient;    // Ambientní komponenta světla; ovlivňuje celkovou barvu a intenzitu světla bez ohledu na směr
    vec3  diffuse;    // Difuzní komponenta světla; ovlivňuje, jak světlo interaguje s povrchem v závislosti na normále povrchu
    vec3  specular;   // Spekulární komponenta světla; způsobuje lesklé odlesky na materiálech s vysokým leskem
    vec3  position;   // Poloha světla ve světových souřadnicích, používá se pro bodová (point) a kuželová (spotlight) světla
    vec3  direction;  // Směr, ve kterém světlo svítí, používá se pro směrová (directional) a kuželová (spotlight) světla
    float cutoff;     // Úhel ořezu pro kuželová světla (spotlights), určuje šířku kuželu světla
    float exponent;   // Exponent osvětlení pro kuželová světla, ovlivňuje rychlost úbytku světla od středu kuželu k jeho okrajům
    float constant;   // Konstantní složka útlumu světla, používá se pro bodová světla, ovlivňuje celkovou intenzitu světla
    float linear;     // Lineární složka útlumu světla, určuje, jak rychle světlo ztrácí intenzitu s rostoucí vzdáleností od zdroje
    float quadratic;  // Kvadratická složka útlumu světla, pro výpočet útlumu světla s kvadratickou závislostí na vzdálenosti
};

uniform Material material;
uniform sampler2D texSampler;

uniform vec3 cameraLightDirection; // Direction the camera light is pointing
uniform vec3 cameraLightPosition;
uniform mat4 Vmatrix;       // View            --> world to eye coordinates
uniform mat4 Mmatrix;       // Model           --> model to world coordinates
uniform mat4 normalMatrix;  // inverse transposed Mmatrix

Light directionalLight = Light(
  vec3(1.0, 1.0, 1.0), // ambient
  vec3(1.0, 1.0, 1.0), // diffuse
  vec3(1.0, 1.0, 1.0), // specular
  vec3(0.0),
  (Vmatrix * vec4(-0.5, -0.866, 0.0, 0.0)).xyz, // position jako směr
  0.0, 0.0, // cutoff a exponent nejsou použity
  1.0, 0.0, 0.0  // konstantní, lineární a kvadratické útlumy
);


uniform bool fog;
uniform float alpha;
uniform bool lamp1;
uniform bool lamp2;
uniform bool lamp3;

// Parametry mlhy
float fogDensity;  // Hustota mlhy
float fogGradient; // Jak rychle se mlha stává hustší s vzdáleností
vec3 fogColor;     // Barva mlhy

Light cameraLight = Light(
  vec3(1.0, 1.0, 1.0),
  vec3(0.8, 0.8, 0.8),
  vec3(1.0, 1.0, 1.0),
  vec3(0.0),
  (Vmatrix * vec4(cameraLightDirection, 0.0)).xyz,
  cos(radians(12.5)), // cutoff
  50.0, // exponent
  1.0, 0.0, 0.0  // konstantní, lineární a kvadratické útlumy
);

Light pointLight = Light(
    vec3(0.3, 0.3, 0.3), // ambient
    vec3(0.7, 0.7, 0.7), // diffuse
    vec3(1.0, 1.0, 1.0), // specular
    vec3((Vmatrix * vec4(0.3, 0.35, 0.5, 1.0)).xyz), // position
    vec3(0.0), // direction, not used for point lights
    0.0, // cutoff, not used for point lights
    0.0, // exponent, not used for point lights
    1.0, // constant attenuation
    0.4, // linear attenuation
    0.8 // quadratic attenuation
);

void initCameraLight() {

  // Nastavení pro mlhu
  fogColor = vec3(0.8, 0.8, 0.8);  // Bílošedivá barva mlhy
  fogDensity = 0.15;               // Hustota mlhy
  fogGradient = 0.35;               // Gradient mlhy

}

in vec3 fragPosition;
in vec3 fragNormal;
in vec2 fragTexCoord;
in vec3 normal;

out vec4 fragColor;

vec4 calculateLighting(Light light, vec3 position, vec3 normal) {
  vec3 lightDir = normalize(light.position - position);
  float diff = max(dot(normal, lightDir), 0.0);
  vec3 reflectDir = reflect(-lightDir, normal);
  vec3 viewDir = normalize(-position);
  float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
  
  vec3 ambient = light.ambient * material.ambient;
  vec3 diffuse = light.diffuse * material.diffuse * diff;
  vec3 specular = light.specular * material.specular * spec;

  vec4 result = vec4(ambient + diffuse + specular, 1.0);

  return result;
}

vec4 calculateSpotlight(Light light, vec3 position, vec3 normal) {
    vec3 lightDir = normalize(light.position - position);
    float theta = dot(lightDir, normalize(-light.direction));

    if (theta > light.cutoff) {
        float diff = max(dot(normal, lightDir), 0.0);
        vec3 reflectDir = reflect(-lightDir, normal);
        vec3 viewDir = normalize(-position);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

        // Default to material colors or use a neutral multiplier if a texture is used
        vec3 effectiveAmbient = material.useTexture ? vec3(1.0) : material.ambient;
        vec3 effectiveDiffuse = material.useTexture ? vec3(1.0) : material.diffuse;
        vec3 effectiveSpecular = material.useTexture ? vec3(1.0) : material.specular;

        vec3 ambient = light.ambient * effectiveAmbient;
        vec3 diffuse = light.diffuse * effectiveDiffuse * diff;
        vec3 specular = light.specular * effectiveSpecular * spec;
        float intensity = pow(theta, light.exponent);

        // Apply texture color to the final output if texture is used
        vec3 colorOutput = ambient + diffuse + specular;
        if (texture(texSampler, fragTexCoord).rgb * material.diffuse != vec3(0.0f)) {
            colorOutput *= texture(texSampler, fragTexCoord).rgb;
        }

        return vec4(colorOutput * intensity, 1.0);
    } else {
        return vec4(0.0);  // No light contribution if outside the spotlight cone
    }
}

vec4 calculatePointLight(Light light, vec3 position, vec3 normal) {
    vec3 lightDir = normalize(light.position - position);
    float diff = max(dot(normal, lightDir), 0.0);
    vec3 reflectDir = reflect(-lightDir, normal);
    vec3 viewDir = normalize(-position);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

    float distance = length(light.position - position);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * distance * distance);

    vec3 ambient = light.ambient * material.ambient;
    vec3 diffuse = light.diffuse * material.diffuse * diff * attenuation;
    vec3 specular = light.specular * material.specular * spec * attenuation;

    return vec4(ambient + diffuse + specular, 1.0);
}

float calculateFogFactor(vec3 position) {
  float distance = length(position - cameraLight.position);
  float fogFactor = exp(-pow(distance * fogDensity, fogGradient));
  return clamp(fogFactor, 0.0, 1.0);
}

void main() {
    initCameraLight();
    vec3 position = fragPosition;
    vec3 normal = fragNormal;

    // Výpočet osvětlení
    vec4 lighting = calculateLighting(directionalLight, fragPosition, fragNormal);
    vec4 pointLighting = vec4(0.0);
    if (lamp1)
        pointLighting = calculatePointLight(pointLight, fragPosition, fragNormal);
      
    vec4 cameraSpotLight = calculateSpotlight(cameraLight, fragPosition, fragNormal);

    vec3 baseColor;

    if (texture(texSampler, fragTexCoord).rgb * material.diffuse != vec3(0.0f)) {
        baseColor = texture(texSampler, fragTexCoord).rgb * material.diffuse;
    } else {
        baseColor = material.diffuse;
    }

    vec4 colorWithLighting = vec4(baseColor, 1.0) * (pointLighting + lighting + cameraSpotLight);

    vec3 finalColor = colorWithLighting.rgb;

    if (fog) {
        float fogFactor = calculateFogFactor(position);
        finalColor = mix(fogColor, finalColor, fogFactor);
    }

    fragColor = vec4(finalColor, alpha);
}
