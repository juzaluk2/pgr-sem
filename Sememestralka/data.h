//----------------------------------------------------------------------------------------
/**
 * \file    data.h
 * \author  Jaroslav Sloup, Tomas Barak, Petr Felkel
 * \date    2013
 * \brief   Basic defines and data structures.
 */
//----------------------------------------------------------------------------------------

#ifndef __DATA_H
#define __DATA_H

#define WINDOW_WIDTH   2400
#define WINDOW_HEIGHT  1000
#define WINDOW_TITLE   "Asteroids Game"

// keys used in the key map
enum { KEY_LEFT_ARROW, KEY_RIGHT_ARROW, KEY_UP_ARROW, KEY_DOWN_ARROW, KEY_SPACE, KEYS_COUNT };

#define ASTEROIDS_COUNT_MIN  5
#define ASTEROIDS_COUNT_MAX 10
#define ASTEROID_PARTS       3

#define ASTEROID_SIZE        0.05f
#define ASTEROID_SIZE_FACTOR 0.75f
#define ASTEROID_SIZE_MIN    (ASTEROID_SIZE*ASTEROID_SIZE_FACTOR*ASTEROID_SIZE_FACTOR)

#define UFOS_COUNT_MIN 1
#define UFOS_COUNT_MAX 3

#define SPACESHIP_VIEW_ANGLE_DELTA 2.0f // in degrees

#define MISSILE_MAX_DISTANCE       1.5f
#define MISSILE_LAUNCH_TIME_DELAY  0.25f // seconds

#define SPACESHIP_SIZE   0.05f
#define UFO_SIZE         0.05f
#define MISSILE_SIZE     0.0085f
#define BILLBOARD_SIZE   0.1f
#define BANNER_SIZE      1.0f

#define MISSILE_SPEED              1.5f
#define ASTEROID_SPEED_MAX         0.5f
#define SPACESHIP_SPEED_INCREMENT  0.025f
#define SPACESHIP_SPEED_MAX        1.0f

#define ASTEROID_ROTATION_SPEED_MAX 1.0f
#define UFO_ROTATION_SPEED_MAX      1.0f

#define SCENE_WIDTH  1.0f
#define SCENE_HEIGHT 1.0f
#define SCENE_DEPTH  1.0f

#define CAMERA_ELEVATION_MAX 45.0f

// default shaders - color per vertex and matrix multiplication
const std::string colorVertexShaderSrc(
    "#version 140\n"
    "uniform mat4 PVMmatrix;\n"
    "in vec3 position;\n"
    "in vec3 color;\n"
    "smooth out vec4 theColor;\n"
    "void main() {\n"
	"  gl_Position = PVMmatrix * vec4(position, 1.0);\n"
	"  theColor = vec4(color, 1.0);\n"
    "}\n"
);

const std::string colorFragmentShaderSrc(
    "#version 140\n"
    "smooth in vec4 theColor;\n"
    "out vec4 outputColor;\n"
    "void main() {\n"
    "  outputColor = theColor;\n"
    "}\n"
);

// each vertex shader receives screen space coordinates and calculates world direction
const std::string skyboxFarPlaneVertexShaderSrc(
  "#version 140\n"
  "\n"
  "uniform mat4 inversePVmatrix;\n"
  "in vec2 screenCoord;\n"
  "out vec3 texCoord_v;\n"
  "\n"
  "void main() {\n"
  "  vec4 farplaneCoord = vec4(screenCoord, 0.9999, 1.0);\n"
  "  vec4 worldViewCoord = inversePVmatrix * farplaneCoord;\n"
  "  texCoord_v = worldViewCoord.xyz / worldViewCoord.w;\n"
  "  gl_Position = farplaneCoord;\n"
  "}\n"
);

// fragment shader uses interpolated 3D tex coords to sample cube map
const std::string skyboxFarPlaneFragmentShaderSrc(
  "#version 140\n"
  "\n"
  "uniform samplerCube skyboxSampler;\n"
  "in vec3 texCoord_v;\n"
  "out vec4 color_f;\n"
  "\n"
  "void main() {\n"
  "  color_f = texture(skyboxSampler, texCoord_v);\n"
  "}\n"
);

//
// missile geometry definition 
//

const int missileTrianglesCount = 4;
// temp constants used for missileVertices array contents definition
const float invSqrt2 = (float)(1.0 / sqrt(2.0));

// missile is represented as a tetrahedron (polyhedron with 4 faces and 4 vertices)
const float missileVertices[] = {
  // non-interleaved array
  // vertices of tetrahedron, each face is an equilateral triangle, edge length 2.0

  // vertices
  //  1.0f,  0.0f, -invSqrt2,   -> vertex 0
  //  0.0f,  1.0f,  invSqrt2,   -> vertex 1
  // -1.0f,  0.0f, -invSqrt2,   -> vertex 2
  //  0.0f, -1.0f,  invSqrt2    -> vertex 3

  // three vertices per each triangle
   0.0f, -1.0f,  invSqrt2, // 3
   1.0f,  0.0f, -invSqrt2, // 0
   0.0f,  1.0f,  invSqrt2, // 1

  -1.0f,  0.0f, -invSqrt2, // 2
   0.0f, -1.0f,  invSqrt2, // 3
   0.0f,  1.0f,  invSqrt2, // 1

   1.0f,  0.0f, -invSqrt2, // 0
  -1.0f,  0.0f, -invSqrt2, // 2
   0.0f,  1.0f,  invSqrt2, // 1

  -1.0f,  0.0f, -invSqrt2, // 2
   1.0f,  0.0f, -invSqrt2, // 0
   0.0f, -1.0f,  invSqrt2, // 3

  // colors for vertices
  1.0f, 0.0f, 1.0f, // 3
  1.0f, 0.0f, 1.0f, // 0
  0.0f, 1.0f, 0.0f, // 1

  0.0f, 0.0f, 1.0f, // 2
  1.0f, 0.0f, 1.0f, // 3
  0.0f, 1.0f, 0.0f, // 1

  1.0f, 0.0f, 1.0f, // 0
  0.0f, 0.0f, 1.0f, // 2
  0.0f, 1.0f, 0.0f, // 1

  0.0f, 0.0f, 1.0f, // 2
  1.0f, 0.0f, 1.0f, // 0
  1.0f, 0.0f, 1.0f, // 3

  // normals
   1.0f, 0.0f, invSqrt2,
   1.0f, 0.0f, invSqrt2,
   1.0f, 0.0f, invSqrt2,

  -1.0f, 0.0f, invSqrt2,
  -1.0f, 0.0f, invSqrt2,
  -1.0f, 0.0f, invSqrt2,

   0.0f, 1.0f, -invSqrt2,
   0.0f, 1.0f, -invSqrt2,
   0.0f, 1.0f, -invSqrt2,

   0.0f, -1.0f, -invSqrt2,
   0.0f, -1.0f, -invSqrt2,
   0.0f, -1.0f, -invSqrt2,
};

//
// ufo geometry definition 
//

const int ufoTrianglesCount = 6;
// temp constants used for ufoVertices array contents definition
const float ufoH = 0.25f;
const float cos30d = (float)cos(M_PI/6.0);
const float sin30d = (float)sin(M_PI/6.0);

const float ufoVertices[] = {
  // ufo is formed by two parts (top and bottom) joined together
  // each part is drawn as six triangles connected together

  // drawArrays() part of data (top part), interleaved array
  // colors of the triangles alternate between yellow (1,1,0) and magenta (1,0,1)

  // vertices 0..5 are on the border, vertex 6 is in the center
  // interleaved array: position/color/normal
  //  x      y        z     r     g     b             nx     ny         nz
  // triangle 5 0 6 -> yellow color
   cos30d, 0.0f, -sin30d,  1.0f, 1.0f, 0.0f,          ufoH, 1.0f,         0.0f, // 5
     0.0f, ufoH,    0.0f,  1.0f, 1.0f, 0.0f,          ufoH, 1.0f,         0.0f, // 6
   cos30d, 0.0f,  sin30d,  1.0f, 1.0f, 0.0f,          ufoH, 1.0f,         0.0f, // 0
  // triangle 1 2 6 -> yellow color
     0.0f, 0.0f,    1.0f,  1.0f, 1.0f, 0.0f,  -ufoH*sin30d, 1.0f,  ufoH*cos30d, // 1
     0.0f, ufoH,    0.0f,  1.0f, 1.0f, 0.0f,  -ufoH*sin30d, 1.0f,  ufoH*cos30d, // 6
  -cos30d, 0.0f,  sin30d,  1.0f, 1.0f, 0.0f,  -ufoH*sin30d, 1.0f,  ufoH*cos30d, // 2
  // triangle 3 4 6 -> yellow color
  -cos30d, 0.0f, -sin30d,  1.0f, 1.0f, 0.0f,  -ufoH*sin30d, 1.0f, -ufoH*cos30d, // 3
     0.0f, ufoH,    0.0f,  1.0f, 1.0f, 0.0f,  -ufoH*sin30d, 1.0f, -ufoH*cos30d, // 6
     0.0f, 0.0f,   -1.0f,  1.0f, 1.0f, 0.0f,  -ufoH*sin30d, 1.0f, -ufoH*cos30d, // 4

  // triangle 0 1 6 -> magenta color
   cos30d, 0.0f,  sin30d,  1.0f, 0.0f, 1.0f,   ufoH*sin30d, 1.0f,  ufoH*cos30d, // 0
     0.0f, ufoH,    0.0f,  1.0f, 0.0f, 1.0f,   ufoH*sin30d, 1.0f,  ufoH*cos30d, // 6
     0.0f, 0.0f,    1.0f,  1.0f, 0.0f, 1.0f,   ufoH*sin30d, 1.0f,  ufoH*cos30d, // 1
  // triangle 2 3 6 -> magenta color
  -cos30d, 0.0f,  sin30d,  1.0f, 0.0f, 1.0f,         -ufoH, 1.0f,         0.0f, // 2
     0.0f, ufoH,    0.0f,  1.0f, 0.0f, 1.0f,         -ufoH, 1.0f,         0.0f, // 6
  -cos30d, 0.0f, -sin30d,  1.0f, 0.0f, 1.0f,         -ufoH, 1.0f,         0.0f, // 3
  // triangle 4 5 6 -> magenta color
     0.0f, 0.0f,   -1.0f,  1.0f, 0.0f, 1.0f,   ufoH*sin30d, 1.0f, -ufoH*cos30d, // 4
     0.0f, ufoH,    0.0f,  1.0f, 0.0f, 1.0f,   ufoH*sin30d, 1.0f, -ufoH*cos30d, // 6
   cos30d, 0.0f, -sin30d,  1.0f, 0.0f, 1.0f,   ufoH*sin30d, 1.0f, -ufoH*cos30d, // 5

  // drawElements() data part (bottom part), interleaved array
  // vertices on the border have the same color while vertex in the middle differs

  // one new yellow vertex in the center of the bottom part
  // vertices on the border are shared with the top part
  //  x     y     z      r     g     b      nx    ny     nz
    0.0f, -ufoH, 0.0f,  1.0f, 1.0f, 0.0f,  0.0f, -1.0f, 0.0f,

};

// indices of the triangle fan used to draw the bottom part of the ufo
const unsigned int ufoIndices[] = {

  18, 9, 11, 12, 14, 15, 17, 9

};


const int stoneTrianglesCount = 102;
const float stoneVertices[] = {
5.935, 0.869, -11.838, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.893, 1.059, -12.052, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.886, 1.063, -12.078, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.935, 0.869, -11.838, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.886, 1.063, -12.078, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.873, 1.061, -12.123, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.935, 0.869, -11.838, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.873, 1.061, -12.123, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.935, 0.869, -11.838, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.935, 0.869, -11.838, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.749, 0.474, -12.324, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.935, 0.869, -11.838, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.749, 0.474, -12.324, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
5.801, 0.443, -12.132, 0.800, 0.800, 0.800, -0.954, 0.106, 0.279,
6.066, 0.776, -12.632, 0.800, 0.800, 0.800, -0.172, 0.289, -0.942,
6.101, 0.863, -12.612, 0.800, 0.800, 0.800, -0.172, 0.289, -0.942,
6.110, 0.863, -12.613, 0.800, 0.800, 0.800, -0.172, 0.289, -0.942,
6.066, 0.776, -12.632, 0.800, 0.800, 0.800, -0.172, 0.289, -0.942,
6.110, 0.863, -12.613, 0.800, 0.800, 0.800, -0.172, 0.289, -0.942,
6.106, 0.775, -12.640, 0.800, 0.800, 0.800, -0.172, 0.289, -0.942,
6.436, 0.793, -11.830, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.062, 0.812, -11.792, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.273, 0.359, -11.894, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.436, 0.793, -11.830, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.273, 0.359, -11.894, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.326, 0.354, -11.900, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.436, 0.793, -11.830, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.326, 0.354, -11.900, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.361, 0.374, -11.900, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.436, 0.793, -11.830, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.361, 0.374, -11.900, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.458, 0.754, -11.839, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.436, 0.793, -11.830, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.458, 0.754, -11.839, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.458, 0.755, -11.839, 0.800, 0.800, 0.800, 0.092, -0.179, 0.980,
6.080, 0.539, -12.607, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.139, 0.741, -12.643, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.160, 0.774, -12.639, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.080, 0.539, -12.607, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.160, 0.774, -12.639, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.172, 0.787, -12.635, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.080, 0.539, -12.607, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.172, 0.787, -12.635, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.593, 0.698, -12.249, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.080, 0.539, -12.607, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.593, 0.698, -12.249, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.477, 0.376, -12.210, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.080, 0.539, -12.607, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.477, 0.376, -12.210, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
6.096, 0.465, -12.562, 0.800, 0.800, 0.800, 0.607, -0.308, -0.733,
5.765, 0.477, -12.364, 0.800, 0.800, 0.800, -0.448, -0.531, -0.719,
5.755, 0.519, -12.389, 0.800, 0.800, 0.800, -0.448, -0.531, -0.719,
5.957, 0.626, -12.595, 0.800, 0.800, 0.800, -0.448, -0.531, -0.719,
5.765, 0.477, -12.364, 0.800, 0.800, 0.800, -0.448, -0.531, -0.719,
5.957, 0.626, -12.595, 0.800, 0.800, 0.800, -0.448, -0.531, -0.719,
6.080, 0.539, -12.607, 0.800, 0.800, 0.800, -0.448, -0.531, -0.719,
5.765, 0.477, -12.364, 0.800, 0.800, 0.800, -0.448, -0.531, -0.719,
6.080, 0.539, -12.607, 0.800, 0.800, 0.800, -0.448, -0.531, -0.719,
6.096, 0.465, -12.562, 0.800, 0.800, 0.800, -0.448, -0.531, -0.719,
6.458, 0.754, -11.839, 0.800, 0.800, 0.800, 0.691, 0.074, 0.719,
6.459, 0.755, -11.840, 0.800, 0.800, 0.800, 0.691, 0.074, 0.719,
6.458, 0.755, -11.839, 0.800, 0.800, 0.800, 0.691, 0.074, 0.719,
6.106, 0.772, -12.640, 0.800, 0.800, 0.800, 0.020, 0.100, -0.995,
6.106, 0.775, -12.640, 0.800, 0.800, 0.800, 0.020, 0.100, -0.995,
6.160, 0.774, -12.639, 0.800, 0.800, 0.800, 0.020, 0.100, -0.995,
6.106, 0.772, -12.640, 0.800, 0.800, 0.800, 0.020, 0.100, -0.995,
6.160, 0.774, -12.639, 0.800, 0.800, 0.800, 0.020, 0.100, -0.995,
6.139, 0.741, -12.643, 0.800, 0.800, 0.800, 0.020, 0.100, -0.995,
6.581, 0.828, -12.107, 0.800, 0.800, 0.800, 0.943, 0.268, 0.196,
6.587, 0.823, -12.127, 0.800, 0.800, 0.800, 0.943, 0.268, 0.196,
6.583, 0.832, -12.122, 0.800, 0.800, 0.800, 0.943, 0.268, 0.196,
6.110, 0.863, -12.613, 0.800, 0.800, 0.800, 0.026, 0.467, -0.884,
6.111, 0.888, -12.600, 0.800, 0.800, 0.800, 0.026, 0.467, -0.884,
6.122, 0.863, -12.613, 0.800, 0.800, 0.800, 0.026, 0.467, -0.884,
6.101, 0.863, -12.612, 0.800, 0.800, 0.800, -0.156, 0.469, -0.869,
6.108, 0.897, -12.595, 0.800, 0.800, 0.800, -0.156, 0.469, -0.869,
6.111, 0.888, -12.600, 0.800, 0.800, 0.800, -0.156, 0.469, -0.869,
6.101, 0.863, -12.612, 0.800, 0.800, 0.800, -0.156, 0.469, -0.869,
6.111, 0.888, -12.600, 0.800, 0.800, 0.800, -0.156, 0.469, -0.869,
6.110, 0.863, -12.613, 0.800, 0.800, 0.800, -0.156, 0.469, -0.869,
5.893, 1.059, -12.052, 0.800, 0.800, 0.800, -0.584, 0.754, 0.301,
5.896, 1.064, -12.059, 0.800, 0.800, 0.800, -0.584, 0.754, 0.301,
5.888, 1.065, -12.078, 0.800, 0.800, 0.800, -0.584, 0.754, 0.301,
5.893, 1.059, -12.052, 0.800, 0.800, 0.800, -0.584, 0.754, 0.301,
5.888, 1.065, -12.078, 0.800, 0.800, 0.800, -0.584, 0.754, 0.301,
5.886, 1.063, -12.078, 0.800, 0.800, 0.800, -0.584, 0.754, 0.301,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.890, -0.263, -0.373,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.890, -0.263, -0.373,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.890, -0.263, -0.373,
5.886, 1.063, -12.078, 0.800, 0.800, 0.800, -0.607, 0.783, 0.135,
5.888, 1.065, -12.078, 0.800, 0.800, 0.800, -0.607, 0.783, 0.135,
5.880, 1.067, -12.128, 0.800, 0.800, 0.800, -0.607, 0.783, 0.135,
5.886, 1.063, -12.078, 0.800, 0.800, 0.800, -0.607, 0.783, 0.135,
5.880, 1.067, -12.128, 0.800, 0.800, 0.800, -0.607, 0.783, 0.135,
5.873, 1.061, -12.123, 0.800, 0.800, 0.800, -0.607, 0.783, 0.135,
6.361, 0.374, -11.900, 0.800, 0.800, 0.800, 0.416, -0.735, 0.535,
6.326, 0.354, -11.900, 0.800, 0.800, 0.800, 0.416, -0.735, 0.535,
6.366, 0.354, -11.932, 0.800, 0.800, 0.800, 0.416, -0.735, 0.535,
6.096, 0.465, -12.562, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
6.477, 0.376, -12.210, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
6.366, 0.354, -11.932, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
6.096, 0.465, -12.562, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
6.366, 0.354, -11.932, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
6.326, 0.354, -11.900, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
6.096, 0.465, -12.562, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
6.326, 0.354, -11.900, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
6.273, 0.359, -11.894, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
6.096, 0.465, -12.562, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
6.273, 0.359, -11.894, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
5.801, 0.443, -12.132, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
6.096, 0.465, -12.562, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
5.801, 0.443, -12.132, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
5.749, 0.474, -12.324, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
6.096, 0.465, -12.562, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
5.749, 0.474, -12.324, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
5.765, 0.477, -12.364, 0.800, 0.800, 0.800, -0.112, -0.986, -0.126,
5.801, 0.443, -12.132, 0.800, 0.800, 0.800, -0.468, -0.397, 0.789,
6.273, 0.359, -11.894, 0.800, 0.800, 0.800, -0.468, -0.397, 0.789,
6.062, 0.812, -11.792, 0.800, 0.800, 0.800, -0.468, -0.397, 0.789,
5.801, 0.443, -12.132, 0.800, 0.800, 0.800, -0.468, -0.397, 0.789,
6.062, 0.812, -11.792, 0.800, 0.800, 0.800, -0.468, -0.397, 0.789,
5.935, 0.869, -11.838, 0.800, 0.800, 0.800, -0.468, -0.397, 0.789,
6.482, 0.845, -11.898, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.513, 0.829, -11.931, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.518, 0.827, -11.937, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.482, 0.845, -11.898, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.518, 0.827, -11.937, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.581, 0.828, -12.107, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.482, 0.845, -11.898, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.581, 0.828, -12.107, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.583, 0.832, -12.122, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.482, 0.845, -11.898, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.583, 0.832, -12.122, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.349, 1.050, -12.162, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.482, 0.845, -11.898, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.349, 1.050, -12.162, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.331, 1.049, -12.112, 0.800, 0.800, 0.800, 0.638, 0.730, 0.246,
6.491, 0.802, -11.887, 0.800, 0.800, 0.800, 0.676, 0.231, 0.700,
6.488, 0.819, -11.889, 0.800, 0.800, 0.800, 0.676, 0.231, 0.700,
6.469, 0.825, -11.873, 0.800, 0.800, 0.800, 0.676, 0.231, 0.700,
6.491, 0.802, -11.887, 0.800, 0.800, 0.800, 0.676, 0.231, 0.700,
6.469, 0.825, -11.873, 0.800, 0.800, 0.800, 0.676, 0.231, 0.700,
6.436, 0.793, -11.830, 0.800, 0.800, 0.800, 0.676, 0.231, 0.700,
6.491, 0.802, -11.887, 0.800, 0.800, 0.800, 0.676, 0.231, 0.700,
6.436, 0.793, -11.830, 0.800, 0.800, 0.800, 0.676, 0.231, 0.700,
6.458, 0.755, -11.839, 0.800, 0.800, 0.800, 0.676, 0.231, 0.700,
6.491, 0.802, -11.887, 0.800, 0.800, 0.800, 0.676, 0.231, 0.700,
6.458, 0.755, -11.839, 0.800, 0.800, 0.800, 0.676, 0.231, 0.700,
6.459, 0.755, -11.840, 0.800, 0.800, 0.800, 0.676, 0.231, 0.700,
6.488, 0.819, -11.889, 0.800, 0.800, 0.800, 0.665, 0.364, 0.652,
6.481, 0.843, -11.896, 0.800, 0.800, 0.800, 0.665, 0.364, 0.652,
6.469, 0.825, -11.873, 0.800, 0.800, 0.800, 0.665, 0.364, 0.652,
6.513, 0.829, -11.931, 0.800, 0.800, 0.800, 0.758, 0.366, 0.539,
6.482, 0.845, -11.898, 0.800, 0.800, 0.800, 0.758, 0.366, 0.539,
6.481, 0.843, -11.896, 0.800, 0.800, 0.800, 0.758, 0.366, 0.539,
6.513, 0.829, -11.931, 0.800, 0.800, 0.800, 0.758, 0.366, 0.539,
6.481, 0.843, -11.896, 0.800, 0.800, 0.800, 0.758, 0.366, 0.539,
6.488, 0.819, -11.889, 0.800, 0.800, 0.800, 0.758, 0.366, 0.539,
6.481, 0.843, -11.896, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.482, 0.845, -11.898, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.331, 1.049, -12.112, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.481, 0.843, -11.896, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.331, 1.049, -12.112, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
5.896, 1.064, -12.059, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.481, 0.843, -11.896, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
5.896, 1.064, -12.059, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
5.893, 1.059, -12.052, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.481, 0.843, -11.896, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
5.893, 1.059, -12.052, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
5.935, 0.869, -11.838, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.481, 0.843, -11.896, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
5.935, 0.869, -11.838, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.062, 0.812, -11.792, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.481, 0.843, -11.896, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.062, 0.812, -11.792, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.436, 0.793, -11.830, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.481, 0.843, -11.896, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.436, 0.793, -11.830, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.469, 0.825, -11.873, 0.800, 0.800, 0.800, 0.105, 0.755, 0.647,
6.593, 0.698, -12.249, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.587, 0.823, -12.127, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.581, 0.828, -12.107, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.593, 0.698, -12.249, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.581, 0.828, -12.107, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.518, 0.827, -11.937, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.593, 0.698, -12.249, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.518, 0.827, -11.937, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.491, 0.802, -11.887, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.593, 0.698, -12.249, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.491, 0.802, -11.887, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.459, 0.755, -11.840, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.593, 0.698, -12.249, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.459, 0.755, -11.840, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.458, 0.754, -11.839, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.593, 0.698, -12.249, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.458, 0.754, -11.839, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.361, 0.374, -11.900, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.593, 0.698, -12.249, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.361, 0.374, -11.900, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.366, 0.354, -11.932, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.593, 0.698, -12.249, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.366, 0.354, -11.932, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.477, 0.376, -12.210, 0.800, 0.800, 0.800, 0.899, -0.284, 0.334,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, 0.038, 0.999, 0.039,
5.880, 1.067, -12.128, 0.800, 0.800, 0.800, 0.038, 0.999, 0.039,
5.888, 1.065, -12.078, 0.800, 0.800, 0.800, 0.038, 0.999, 0.039,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, 0.038, 0.999, 0.039,
5.888, 1.065, -12.078, 0.800, 0.800, 0.800, 0.038, 0.999, 0.039,
5.896, 1.064, -12.059, 0.800, 0.800, 0.800, 0.038, 0.999, 0.039,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, 0.038, 0.999, 0.039,
5.896, 1.064, -12.059, 0.800, 0.800, 0.800, 0.038, 0.999, 0.039,
6.331, 1.049, -12.112, 0.800, 0.800, 0.800, 0.038, 0.999, 0.039,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, 0.038, 0.999, 0.039,
6.331, 1.049, -12.112, 0.800, 0.800, 0.800, 0.038, 0.999, 0.039,
6.349, 1.050, -12.162, 0.800, 0.800, 0.800, 0.038, 0.999, 0.039,
6.491, 0.802, -11.887, 0.800, 0.800, 0.800, 0.814, 0.215, 0.539,
6.518, 0.827, -11.937, 0.800, 0.800, 0.800, 0.814, 0.215, 0.539,
6.513, 0.829, -11.931, 0.800, 0.800, 0.800, 0.814, 0.215, 0.539,
6.491, 0.802, -11.887, 0.800, 0.800, 0.800, 0.814, 0.215, 0.539,
6.513, 0.829, -11.931, 0.800, 0.800, 0.800, 0.814, 0.215, 0.539,
6.488, 0.819, -11.889, 0.800, 0.800, 0.800, 0.814, 0.215, 0.539,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.349, 1.050, -12.162, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.583, 0.832, -12.122, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.583, 0.832, -12.122, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.587, 0.823, -12.127, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.587, 0.823, -12.127, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.593, 0.698, -12.249, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.593, 0.698, -12.249, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.172, 0.787, -12.635, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.172, 0.787, -12.635, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.122, 0.863, -12.613, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.122, 0.863, -12.613, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.111, 0.888, -12.600, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.111, 0.888, -12.600, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.108, 0.897, -12.595, 0.800, 0.800, 0.800, 0.620, 0.563, -0.546,
6.106, 0.775, -12.640, 0.800, 0.800, 0.800, 0.023, 0.286, -0.958,
6.110, 0.863, -12.613, 0.800, 0.800, 0.800, 0.023, 0.286, -0.958,
6.122, 0.863, -12.613, 0.800, 0.800, 0.800, 0.023, 0.286, -0.958,
6.106, 0.775, -12.640, 0.800, 0.800, 0.800, 0.023, 0.286, -0.958,
6.122, 0.863, -12.613, 0.800, 0.800, 0.800, 0.023, 0.286, -0.958,
6.172, 0.787, -12.635, 0.800, 0.800, 0.800, 0.023, 0.286, -0.958,
6.106, 0.775, -12.640, 0.800, 0.800, 0.800, 0.023, 0.286, -0.958,
6.172, 0.787, -12.635, 0.800, 0.800, 0.800, 0.023, 0.286, -0.958,
6.160, 0.774, -12.639, 0.800, 0.800, 0.800, 0.023, 0.286, -0.958,
6.064, 0.772, -12.633, 0.800, 0.800, 0.800, -0.181, 0.104, -0.978,
6.066, 0.776, -12.632, 0.800, 0.800, 0.800, -0.181, 0.104, -0.978,
6.106, 0.775, -12.640, 0.800, 0.800, 0.800, -0.181, 0.104, -0.978,
6.064, 0.772, -12.633, 0.800, 0.800, 0.800, -0.181, 0.104, -0.978,
6.106, 0.775, -12.640, 0.800, 0.800, 0.800, -0.181, 0.104, -0.978,
6.106, 0.772, -12.640, 0.800, 0.800, 0.800, -0.181, 0.104, -0.978,
6.080, 0.539, -12.607, 0.800, 0.800, 0.800, -0.181, -0.121, -0.976,
5.957, 0.626, -12.595, 0.800, 0.800, 0.800, -0.181, -0.121, -0.976,
6.064, 0.772, -12.633, 0.800, 0.800, 0.800, -0.181, -0.121, -0.976,
6.080, 0.539, -12.607, 0.800, 0.800, 0.800, -0.181, -0.121, -0.976,
6.064, 0.772, -12.633, 0.800, 0.800, 0.800, -0.181, -0.121, -0.976,
6.106, 0.772, -12.640, 0.800, 0.800, 0.800, -0.181, -0.121, -0.976,
6.080, 0.539, -12.607, 0.800, 0.800, 0.800, -0.181, -0.121, -0.976,
6.106, 0.772, -12.640, 0.800, 0.800, 0.800, -0.181, -0.121, -0.976,
6.139, 0.741, -12.643, 0.800, 0.800, 0.800, -0.181, -0.121, -0.976,
5.957, 0.626, -12.595, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.755, 0.519, -12.389, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.957, 0.626, -12.595, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.957, 0.626, -12.595, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.873, 1.061, -12.123, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.957, 0.626, -12.595, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.873, 1.061, -12.123, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.880, 1.067, -12.128, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.957, 0.626, -12.595, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.880, 1.067, -12.128, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.957, 0.626, -12.595, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
6.093, 1.071, -12.433, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
6.108, 0.897, -12.595, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.957, 0.626, -12.595, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
6.108, 0.897, -12.595, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
6.101, 0.863, -12.612, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.957, 0.626, -12.595, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
6.101, 0.863, -12.612, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
6.066, 0.776, -12.632, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.957, 0.626, -12.595, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
6.066, 0.776, -12.632, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
6.064, 0.772, -12.633, 0.800, 0.800, 0.800, -0.748, 0.417, -0.516,
5.749, 0.474, -12.324, 0.800, 0.800, 0.800, -0.834, -0.415, -0.364,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.834, -0.415, -0.364,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.834, -0.415, -0.364,
5.749, 0.474, -12.324, 0.800, 0.800, 0.800, -0.834, -0.415, -0.364,
5.742, 0.524, -12.366, 0.800, 0.800, 0.800, -0.834, -0.415, -0.364,
5.755, 0.519, -12.389, 0.800, 0.800, 0.800, -0.834, -0.415, -0.364,
5.749, 0.474, -12.324, 0.800, 0.800, 0.800, -0.834, -0.415, -0.364,
5.755, 0.519, -12.389, 0.800, 0.800, 0.800, -0.834, -0.415, -0.364,
5.765, 0.477, -12.364, 0.800, 0.800, 0.800, -0.834, -0.415, -0.364
};

//
// explosion billboard geometry definition 
//

const int explosionNumQuadVertices = 4;
const float explosionVertexData[explosionNumQuadVertices * 5] = {

  // x      y     z     u     v
  -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
   1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
  -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
   1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
};

//
// "game over" banner geometry definition 
//

const int bannerNumQuadVertices = 4;
const float bannerVertexData[bannerNumQuadVertices * 5] = {

  // x      y      z     u     v
  -1.0f,  0.15f, 0.0f, 0.0f, 1.0f,
  -1.0f, -0.15f, 0.0f, 0.0f, 0.0f,
   1.0f,  0.15f, 0.0f, 3.0f, 1.0f,
   1.0f, -0.15f, 0.0f, 3.0f, 0.0f
};

const int roadBlocksCount = 22;
const glm::vec3 roadBlocksScale = glm::vec3(0.1f);
const float deg36 = glm::radians(36.0f);
const float deg72 = glm::radians(72.0f);
const float deg108 = glm::radians(108.0f);
const float deg144 = glm::radians(144.0f);
const float roadBlocksProperties[roadBlocksCount * 6]{
    // R-x    R-y    R-z    x      y      z      R stands for: Rotation on axis-" "
      0.0f,  0.0f,  0.0f, 0.0f,  0.0f,  0.0f,
      0.0f,  0.0f,  0.0f, 0.0f,  0.0f,  0.89f,
      0.0f,  0.0f,  0.0f, 0.0f,  0.0f,  1.78f,
      0.0f,  0.0f,  0.0f, 0.0f,  0.0f,  2.67f,
      0.0f,  0.0f,  0.0f, 0.0f,  0.0f,  -0.89f,
      0.0f,  0.0f,  0.0f, 0.0f,  0.0f,  -1.78f,
      0.0f,  0.0f,  0.0f, 0.0f,  0.0f,  -2.67f,

      0.0f,  0.0f,  0.0f, -2.3f,  0.0f,  0.0f,
      0.0f,  0.0f,  0.0f, -2.3f,  0.0f,  0.89f,
      0.0f,  0.0f,  0.0f, -2.3f,  0.0f,  1.78f,
      0.0f,  0.0f,  0.0f, -2.3f,  0.0f,  2.67f,
      0.0f,  0.0f,  0.0f, -2.3f,  0.0f,  -0.89f,
      0.0f,  0.0f,  0.0f, -2.3f,  0.0f,  -1.78f,
      0.0f,  0.0f,  0.0f, -2.3f,  0.0f,  -2.67f,

      0.0f,  deg36,  0.0f, -0.22f,  -0.001f, -3.34f,
      0.0f,  deg72,  0.0f, -0.80f,  -0.002f, -3.76f,
      0.0f,  deg108, 0.0f, -1.50f,  -0.003f, -3.76f,
      0.0f,  deg144, 0.0f, -2.08f,  -0.004f, -3.34f,

      0.0f, -deg36,  0.0f, -0.22f,  -0.001f,  3.34f,
      0.0f, -deg72,  0.0f, -0.80f,  -0.002f,  3.76f,
      0.0f, -deg108, 0.0f, -1.50f,  -0.003f,  3.76f,
      0.0f, -deg144, 0.0f, -2.08f,  -0.004f,  3.34f,
};

const int barrelsCount = 3;
#endif // __DATA_H
