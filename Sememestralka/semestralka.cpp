//----------------------------------------------------------------------------------------
/**
 * \file    asteroids.cpp
 * \author  Jaroslav Sloup, Tomas Barak, Petr Felkel
 * \date    2011-2012
 * \brief   Simple implementaion of Asteroids game.
 */
//----------------------------------------------------------------------------------------

#include <time.h>
#include <list>
#include "pgr.h"
#include "render_stuff.h"
#include "spline.h"
#include <iostream>
#include <random>   // Include this for std::uniform_real_distribution and std::mt19937
#include <vector>   // Assuming you need this for managing your game objects



extern SCommonShaderProgram shaderProgram;
extern bool useLighting;

typedef std::list<void *> GameObjectsList; 


const char* CAR = "data/Car/GTO.obj";
const char* CHEVROLET = "data/Chevrolet/Chevrolet_Camaro_SS_High.obj";
const char* F1 = "data/F1/Formula 1 mesh.obj";
const char* ROAD = "data/Road/old road.obj";
const char* Anvil = "data/Anvil/anvil.obj";
const char* BARREL = "data/Barrel/Barrel.obj";
const char* TREE = "data/Tree/Tree.obj";
const char* House = "data/House/Cyprys_House.obj";
const char* LAMP = "data/Lamp/rv_lamp post 1.obj";
const char* ROCK = "data/Rock/Rock1.obj";

struct GameState {

  int windowWidth;    // set by reshape callback
  int windowHeight;   // set by reshape callback

  int CameraMode;// false;
  bool fog;
  float cameraElevationAngle; // in degrees = initially 0.0f

  // New camera variables
  glm::vec3 cameraPosition;   // Add default position
  glm::vec3 cameraDirection;  // Add default direction
  glm::vec3 cameraUpVector;   // Usually glm::vec3(0.0f, 0.0f, 1.0f)

  float cameraYaw;   // Horizontal angle
  float cameraPitch; // Vertical angle
  float cameraSpeed;

  bool gameOver;              // false;
  bool keyMap[KEYS_COUNT];    // false

  float elapsedTime;

  float F1DeltaTime;
  float F1StartTime;

} gameState;

struct GameObjects {

  StaticMesh *Anvil;
  StaticMesh *Grid;
  StaticMeshs* Car1;
  StaticMeshs* Car2;
  StaticMeshs** Lamps;
  StaticMeshs** Trees;
  StaticMeshs* F1;
  StaticMeshs** Barrels;
  StaticMeshs** Roads;
  StaticMeshs** Rocks;
  StaticMeshs** Houses;
  StaticMesh* HardCodedRock;



  GameObjectsList explosions;
  BannerObject* bannerObject; // NULL;
} gameObjects;

void updateCameraDirection();

void insertExplosion(const glm::vec3 &position) {

  ExplosionObject* newExplosion = new ExplosionObject;

  newExplosion->speed = 0.1f;
  newExplosion->destroyed = false;

  newExplosion->startTime = gameState.elapsedTime;
  newExplosion->currentTime = newExplosion->startTime;

  newExplosion->size = BILLBOARD_SIZE;
  newExplosion->direction = glm::vec3(0.0f, 0.0f, 1.0f);

  newExplosion->frameDuration = 0.1f;
  newExplosion->textureFrames = 16;

  newExplosion->position = position;

  gameObjects.explosions.push_back(newExplosion);
}

void addRandomExplosions(glm::vec3 position, float range) {
    std::random_device rd;  // Non-deterministic random number generator
    std::mt19937 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(-range, range); // Uniform distribution from -0.2 to 0.2

    int numExplosions = 3 + rand() % 4; // Random number between 3 and 6
    for (int i = 0; i < numExplosions; ++i) {
        // Generate random offsets
        float offsetX = dis(gen);
        float offsetY = dis(gen);
        float offsetZ = dis(gen);

        // Calculate new position
        glm::vec3 newPosition = position + glm::vec3(offsetX, offsetY, offsetZ);

        // Add explosion at newPosition
        insertExplosion(newPosition);
    }
}

void teleport(void) {
}

void cleanUpObjects(void) {

  // delete explosions
  while(!gameObjects.explosions.empty()) {
    delete gameObjects.explosions.back();
    gameObjects.explosions.pop_back();
  } 

  // remove banner
  if(gameObjects.bannerObject != NULL) {
    delete gameObjects.bannerObject;
    gameObjects.bannerObject = NULL;
  }
}

// generates random position that does not collide with the spaceship
glm::vec3 generateRandomPosition(void) {
 glm::vec3 newPosition;

    // position is generated randomly
    // coordinates are in range -1.0f ... 1.0f
    newPosition = glm::vec3(
      (float)(2.0 * (rand() / (double)RAND_MAX) - 1.0),
      (float)(2.0 * (rand() / (double)RAND_MAX) - 1.0),
      0.0f
    );

  return newPosition;
}

StaticMesh* createStaticMesh(const glm::quat& rotation, const glm::vec3& position, const glm::vec3& scale, const char* path) {
    StaticMesh* staticMesh = new StaticMesh;  // Vytvo?en� nov� instance StaticMesh

    // Nastaven� transforma?n�ch atribut?
    staticMesh->position = position;
    staticMesh->rotation = rotation;
    staticMesh->scale = scale;
    staticMesh->isActive = true; // Nastaven� mesh jako aktivn�, m?�ete p?idat parametr funkce pro toto nastaven�

    // Na?ten� geometrie pro mesh
    if (!loadSingleMesh(path, shaderProgram, &staticMesh->geometry)) {
        std::cerr << "initializeModels(): Anvil model loading failed." << std::endl;
        delete staticMesh; // Uvoln?n� pam?ti, pokud na?ten� sel�e
        return nullptr;
    }

    return staticMesh; // Vr�t� inicializovan� objekt StaticMesh
}

StaticMeshs* createStaticMeshs(const glm::quat& rotation, const glm::vec3& position, const glm::vec3& scale, const char* path, SCommonShaderProgram& shaderProgram) {
    StaticMeshs* staticMesh = new StaticMeshs;  // Vytvo�en� nov� instance StaticMeshs

    // Nastaven� transforma�n�ch atribut�
    staticMesh->position = position;
    staticMesh->rotation = rotation;
    staticMesh->scale = scale;
    staticMesh->isActive = true; // Nastaven� mesh jako aktivn�, m��ete p�idat parametr funkce pro toto nastaven�

    // Na�ten� geometrie pro mesh
    if (!loadGeometry(path, shaderProgram, staticMesh)) {
        std::cerr << "Initialization failed: Model loading failed." << std::endl;
        delete staticMesh; // Uvoln�n� pam�ti, pokud na�ten� sel�e
        return nullptr;
    }

    return staticMesh; // Vr�t� inicializovan� objekt StaticMeshs
}

void restartGame(void) {

  cleanUpObjects();

  gameState.elapsedTime = 0.001f * (float)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds
  
  gameState.CameraMode = 0;
  gameState.cameraElevationAngle = 0.0f;

  gameState.fog = false;
  // reset key map
  for(int i=0; i<KEYS_COUNT; i++)
    gameState.keyMap[i] = false;

  gameState.gameOver = false;
}

BannerObject* createBanner(void) {
 BannerObject* newBanner = new BannerObject;
 
  newBanner->size = BANNER_SIZE;
  newBanner->position = glm::vec3(0.0f, 0.0f, 0.0f);
  newBanner->direction = glm::vec3(0.0f, 1.0f, 0.0f);
  newBanner->speed = 0.0f;
  newBanner->size = 1.0f;

  newBanner->destroyed = false;

  newBanner->startTime = gameState.elapsedTime;
  newBanner->currentTime = newBanner->startTime;

  return newBanner;
}

void setupStencilBuffer() {
    glEnable(GL_STENCIL_TEST);                // Zapnutí stencil testu
    glClear(GL_STENCIL_BUFFER_BIT);           // Vyčištění stencil bufferu
    glStencilFunc(GL_ALWAYS, 1, 0xFF);        // Stencil test vždy projde
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE); // Nastavení stencil operace
}

void drawWindowContents() {

  // setup parallel projection
  const glm::mat4 orthoProjectionMatrix = glm::ortho(
    -SCENE_WIDTH, SCENE_WIDTH,
    -SCENE_HEIGHT, SCENE_HEIGHT,
    -10.0f*SCENE_DEPTH, 10.0f*SCENE_DEPTH
  );
  // static viewpoint - top view
  const glm::mat4 orthoViewMatrix = glm::lookAt(
    glm::vec3(0.0f, 0.0f, 1.0f),
    glm::vec3(0.0f, 0.0f, 0.0f),
    glm::vec3(0.0f, 1.0f, 0.0f)
  );

  // setup camera & projection transform
  glm::mat4 viewMatrix  = orthoViewMatrix;
  glm::mat4 projectionMatrix = orthoProjectionMatrix;

  if(gameState.CameraMode == 0) {

      glm::vec3 cameraCenter = gameState.cameraPosition + gameState.cameraDirection;

      viewMatrix = glm::lookAt(
          gameState.cameraPosition,
          cameraCenter,
          gameState.cameraUpVector
      );

      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth / (float)gameState.windowHeight, 0.1f, 10.0f);
  }
  else if (gameState.CameraMode == 1) {
      glm::vec3 offset = glm::vec3(0.0f, 0.5f, -0.8f);

      // Přidání korekce o 90 stupňů proti směru hodinových ručiček k rotaci auta
      glm::quat correctedRotation = glm::rotate(gameObjects.F1->rotation, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));

      // Aplikace korigované rotace na offset kamery
      offset = correctedRotation * offset;

      gameState.cameraPosition = gameObjects.F1->position + offset;

      glm::vec3 cameraTarget = gameObjects.F1->position + glm::vec3(0.0f, 0.1f, 0.0f);
      gameState.cameraDirection = glm::normalize(cameraTarget - gameState.cameraPosition);
      glm::vec3 cameraCenter = gameState.cameraPosition + gameState.cameraDirection;

      gameState.cameraYaw = glm::degrees(atan2(gameState.cameraDirection.z, gameState.cameraDirection.x));
      gameState.cameraPitch = glm::degrees(asin(gameState.cameraDirection.y));

      viewMatrix = glm::lookAt(
          gameState.cameraPosition,
          cameraCenter,
          gameState.cameraUpVector
      );

      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth / (float)gameState.windowHeight, 0.1f, 100.0f);
  }
  else if (gameState.CameraMode == 2){
      gameState.cameraPosition = glm::vec3(3.0f, 1.0f, -1.0f);
      glm::vec3 cameraTarget = gameObjects.F1->position + glm::vec3(0.0f, -0.1f, 0.0f);
      gameState.cameraDirection = glm::normalize(cameraTarget - gameState.cameraPosition);
      glm::vec3 cameraCenter = gameState.cameraPosition + gameState.cameraDirection;

      gameState.cameraYaw = glm::degrees(atan2(gameState.cameraDirection.z, gameState.cameraDirection.x));
      gameState.cameraPitch = glm::degrees(asin(gameState.cameraDirection.y));

      viewMatrix = glm::lookAt(
          gameState.cameraPosition,
          cameraCenter,
          gameState.cameraUpVector
      );

      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth / (float)gameState.windowHeight, 0.1f, 100.0f);

  } else if (gameState.CameraMode == 3){
      gameState.cameraPosition = glm::vec3(-3.0f, 1.0f, -1.0f);
      glm::vec3 cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
      gameState.cameraDirection = glm::normalize(cameraTarget - gameState.cameraPosition);
      glm::vec3 cameraCenter = gameState.cameraPosition + gameState.cameraDirection;

      gameState.cameraYaw = glm::degrees(atan2(gameState.cameraDirection.z, gameState.cameraDirection.x));
      gameState.cameraPitch = glm::degrees(asin(gameState.cameraDirection.y));

      viewMatrix = glm::lookAt(
          gameState.cameraPosition,
          cameraCenter,
          gameState.cameraUpVector
      );
      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth / (float)gameState.windowHeight, 0.1f, 100.0f);
  } else if (gameState.CameraMode == 4){
      glm::vec3 cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
      gameState.cameraDirection = glm::normalize(cameraTarget - gameState.cameraPosition);
      glm::vec3 cameraCenter = gameState.cameraPosition + gameState.cameraDirection;

      gameState.cameraYaw = glm::degrees(atan2(gameState.cameraDirection.z, gameState.cameraDirection.x));
      gameState.cameraPitch = glm::degrees(asin(gameState.cameraDirection.y));

      viewMatrix = glm::lookAt(
          gameState.cameraPosition,
          cameraCenter,
          gameState.cameraUpVector
      );
      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth / (float)gameState.windowHeight, 0.1f, 100.0f);
  }

  glUseProgram(shaderProgram.program);
  glUniform1f(shaderProgram.timeLocation, gameState.elapsedTime);

  glUseProgram(0);


  

 // draw grid

  drawStaticMesh(gameObjects.Grid, viewMatrix, projectionMatrix);


 for (int i = 0; i < roadBlocksCount; ++i) {
     drawStaticMeshs(gameObjects.Roads[i], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
 }
  // draw cars
  setupStencilBuffer();
  int objectID = 1;
  
  glStencilFunc(GL_ALWAYS, objectID++, 0xFF);
  drawStaticMeshs(gameObjects.Car1, viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  
  glStencilFunc(GL_ALWAYS, objectID++, 0xFF);
  drawStaticMeshs(gameObjects.Car2, viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  
  glStencilFunc(GL_ALWAYS, objectID++, 0xFF);
  drawStaticMeshs(gameObjects.F1, viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  
  // draw Lamps
  glStencilFunc(GL_ALWAYS, objectID++, 0xFF);
  drawStaticMeshs(gameObjects.Lamps[0], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  glStencilFunc(GL_ALWAYS, objectID++, 0xFF);
  drawStaticMeshs(gameObjects.Lamps[1], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  glStencilFunc(GL_ALWAYS, objectID++, 0xFF);
  drawStaticMeshs(gameObjects.Lamps[2], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  glDisable(GL_STENCIL_TEST);
  

  // draw Houses
  drawStaticMeshs(gameObjects.Houses[0], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  drawStaticMeshs(gameObjects.Houses[1], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  
 
  // draw Rocks
  drawStaticMeshs(gameObjects.Rocks[0], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  drawStaticMeshs(gameObjects.Rocks[1], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  drawStaticMeshs(gameObjects.Rocks[2], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  drawStaticMeshs(gameObjects.Rocks[3], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  drawStaticMeshs(gameObjects.Rocks[4], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);

  drawStaticMesh(gameObjects.HardCodedRock, viewMatrix, projectionMatrix);

  // draw Anvil
  drawStaticMesh(gameObjects.Anvil, viewMatrix, projectionMatrix);

  //draw Barrel
  drawStaticMeshs(gameObjects.Barrels[0], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  drawStaticMeshs(gameObjects.Barrels[1], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  drawStaticMeshs(gameObjects.Barrels[2], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);

  // draw skybox
  drawSkybox(viewMatrix, projectionMatrix); 

  // draw explosions with depth test disabled
  glDisable(GL_DEPTH_TEST);

  for (auto it = gameObjects.explosions.begin(); it != gameObjects.explosions.end(); /* no increment here */) {
      ExplosionObject* explosion = static_cast<ExplosionObject*>(*it);
      explosion->currentTime = gameState.elapsedTime;
      if (explosion->currentTime - explosion->startTime > explosion->frameDuration * explosion->textureFrames) {
          // Erase returns the next iterator, so we can continue from it
          it = gameObjects.explosions.erase(it);
      }
      else {
          drawExplosion(explosion, viewMatrix, projectionMatrix);
          ++it;
      }
  }
        

  glEnable(GL_DEPTH_TEST);

  if(gameState.gameOver == true) {
    // draw game over banner
    if(gameObjects.bannerObject != NULL)
      drawBanner(gameObjects.bannerObject, orthoViewMatrix, orthoProjectionMatrix);
  }

  glDisable(GL_STENCIL_TEST);
}

// Called to update the display. You should call glutSwapBuffers after all of your
// rendering to display what you rendered.
void displayCallback() {
  GLbitfield mask = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;

  glClear(mask);

  drawWindowContents();

  glutSwapBuffers();
}

// Called whenever the window is resized. The new window size is given, in pixels.
// This is an opportunity to call glViewport or glScissor to keep up with the change in size.
void reshapeCallback(int newWidth, int newHeight) {

  gameState.windowWidth = newWidth;
  gameState.windowHeight = newHeight;

  glViewport(0, 0, (GLsizei) newWidth, (GLsizei) newHeight);
}

void updateObjects(float elapsedTime) {
    if (gameObjects.F1->playAnimation) {
        float time = elapsedTime - gameState.F1StartTime + gameState.F1DeltaTime;
        gameObjects.F1->position = gameObjects.F1->startPosition + evaluateClosedCurve(curveData, curveSize, time);

        glm::vec3 derivative = evaluateClosedCurve_1stDerivative(curveData, curveSize, time);
        derivative = glm::normalize(derivative);

        // Výpočet úhlu ve směru osy Y na základě derivace křivky
        float angleY = atan2(derivative.x, derivative.z);

        // Přidání korekce úhlu o -90 stupňů (v radiánech), protože model auta je otočený o 90 stupňů doprava
        float correctionAngle = glm::radians(90.0f); // -90 stupňů převedených na radiány
        angleY += correctionAngle;

        // Vytvoření kvaternionu pro rotaci s korekcí
        glm::quat rotation = glm::angleAxis(angleY, glm::vec3(0.0f, 1.0f, 0.0f));

        // Nastavení rotace auta
        gameObjects.F1->rotation = rotation;
    }
    if (gameState.CameraMode == 4){
      gameState.cameraPosition = evaluateClosedCurve(cameraCurveData, cameraCurveSize, elapsedTime/3);
    }
}

void checkBounds(glm::vec3 new_pos) {
    if (new_pos.x >= 8) {
        gameState.cameraPosition.x = 8;
    }
    if (new_pos.x <= -8) {
        gameState.cameraPosition.x = -8;
    }
    if (new_pos.y >= 4) {
        gameState.cameraPosition.y = 4;
    }
    if (new_pos.y <= 0.1) {
        gameState.cameraPosition.y = 0.1;
    }
    if (new_pos.z >= 8) {
        gameState.cameraPosition.z = 8;
    }
    if (new_pos.z <= -8) {
        gameState.cameraPosition.z = -8;
    }
    if (new_pos.x <= 1.6 && new_pos.y <= 0.7 && new_pos.x >= 0.7 && new_pos.z <= 0.6 && new_pos.z >= -0.3) {
        if (abs(new_pos.x - 1.5) < abs(new_pos.x - 0.8))
            gameState.cameraPosition.x += 0.05;
        else
            gameState.cameraPosition.x -= 0.05;
        if (abs(new_pos.z - 0.8) < abs(new_pos.z + 0.2))
            gameState.cameraPosition.z += 0.05;
        else
            gameState.cameraPosition.z -= 0.05;
        gameState.cameraPosition.y += 0.05;
    }
}

// Callback responsible for the scene update
void timerCallback(int) {

  // update scene time
  gameState.elapsedTime = 0.001f * (float)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds

  // call appropriate actions according to the currently pressed keys in key map
  // (combinations of keys are supported but not used in this implementation)
  float cameraSpeed = 0.05f; // Adjust camera speed to your liking
  
  if (gameState.CameraMode == 0)
  {
      if (gameState.keyMap[KEY_UP_ARROW] == true) {
          gameState.cameraPosition += cameraSpeed * gameState.cameraDirection;
          checkBounds(gameState.cameraPosition);
      }

      if (gameState.keyMap[KEY_DOWN_ARROW] == true) {
          gameState.cameraPosition -= cameraSpeed * gameState.cameraDirection;
          checkBounds(gameState.cameraPosition);
      }

      if (gameState.keyMap[KEY_LEFT_ARROW] == true) {
          glm::vec3 cameraRight = glm::normalize(glm::cross(gameState.cameraDirection, gameState.cameraUpVector));
          gameState.cameraPosition -= cameraRight * cameraSpeed;
          checkBounds(gameState.cameraPosition);
      }

      if (gameState.keyMap[KEY_RIGHT_ARROW] == true) {
          glm::vec3 cameraRight = glm::normalize(glm::cross(gameState.cameraDirection, gameState.cameraUpVector));
          gameState.cameraPosition += cameraRight * cameraSpeed;
          checkBounds(gameState.cameraPosition);
      }
  }

  if((gameState.gameOver == true) && (gameObjects.bannerObject != NULL)) {
    gameObjects.bannerObject->currentTime = gameState.elapsedTime;
  }

  // update objects in the scene
  updateObjects(gameState.elapsedTime);

  // space pressed -> launch missile
  if(gameState.keyMap[KEY_SPACE] == true) {

  }

  // game over? -> create banner with scrolling text "game over"
  if(gameState.gameOver == true) {
    gameState.keyMap[KEY_SPACE] = false;
    if(gameObjects.bannerObject == NULL) {
      // if game over and banner still not created -> create banner
      gameObjects.bannerObject = createBanner();
    }
  }

  // set timeCallback next invocation
  glutTimerFunc(33, timerCallback, 0);

  glutPostRedisplay();
}

void updateCameraDirection() {
    // Convert spherical to Cartesian coordinates to update camera direction
    glm::vec3 direction;
    direction.x = cos(glm::radians(gameState.cameraYaw)) * cos(glm::radians(gameState.cameraPitch));
    direction.y = sin(glm::radians(gameState.cameraPitch));
    direction.z = sin(glm::radians(gameState.cameraYaw)) * cos(glm::radians(gameState.cameraPitch));
    gameState.cameraDirection = glm::normalize(direction);
}

void onMouseClickCallback(int button, int state, int x, int y) {

    int correctedY = glutGet(GLUT_WINDOW_HEIGHT) - y - 1;
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
        int stencilValue;
        glReadPixels(x, correctedY, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_INT, &stencilValue);

        if (stencilValue > 0) {
            std::cout << "Object with ID " << stencilValue << " was clicked." << std::endl;
            if (stencilValue == 1) {
                gameObjects.Car1->isActive = false;
                addRandomExplosions(gameObjects.Car1->position, 0.1f);
            }
            else if (stencilValue == 2) {
                gameObjects.Car2->isActive = false;
                addRandomExplosions(gameObjects.Car2->position, 0.1f);
            }
            else if (stencilValue == 3) {
                if (!gameObjects.F1->playAnimation) {
                    gameObjects.F1->playAnimation = true;
                    gameState.F1StartTime = gameState.elapsedTime;
                }
                else {
                    gameObjects.F1->playAnimation = false;
                    gameState.F1DeltaTime += gameState.elapsedTime - gameState.F1StartTime;
                }  
            }
            else if (stencilValue == 4) { // middle one
                gameObjects.Lamps[0]->playAnimation = true;
                setLamps(1, true);
            }
            else if (stencilValue == 5) {
                gameObjects.Lamps[1]->playAnimation = true;
            }
            else if (stencilValue == 6) {
                gameObjects.Lamps[2]->playAnimation = true;
            }
        }
    }
}

// Called when mouse is moving while no mouse buttons are pressed.
void passiveMouseMotionCallback(int mouseX, int mouseY) {

    if (gameState.CameraMode == 0) {
        // Calculate the new angles based on mouse position
        float deltaX = 0.3f * (mouseX - gameState.windowWidth / 2);   // Horizontal change
        float deltaY = 0.3f * (mouseY - gameState.windowHeight / 2);  // Vertical change

        gameState.cameraYaw += deltaX;
        gameState.cameraPitch -= deltaY;

        // Clamp the camera pitch to prevent the camera from flipping over
        gameState.cameraPitch = glm::clamp(gameState.cameraPitch, -85.0f, 85.0f);

        // Update camera direction
        updateCameraDirection();

        // Set the mouse pointer back to the window center to avoid drifting
        glutWarpPointer(gameState.windowWidth / 2, gameState.windowHeight / 2);
    }
}

// Called whenever a key on the keyboard was pressed. The key is given by the "keyPressed"
// parameter, which is in ASCII. It's often a good idea to have the escape key (ASCII value 27)
// to call glutLeaveMainLoop() to exit the program.
void keyboardCallback(unsigned char keyPressed, int mouseX, int mouseY) {
  
  switch(keyPressed) {
    case 27: // escape
#ifndef __APPLE__
        glutLeaveMainLoop();
#else
        exit(0);
#endif
      break;
    case 'r': // restart game
      restartGame();
      break;
    case ' ': // launch missile
      if(gameState.gameOver != true)
        gameState.keyMap[KEY_SPACE] = true;
      break;
    case 't': // switch fog ON/OFF
        if (gameState.fog)
            gameState.fog = false;
        else
            gameState.fog = true;
        setFog(gameState.fog);
      break;
    case 'c': // switch camera
      gameState.CameraMode += 1;
      gameState.CameraMode = gameState.CameraMode % 5;
      if(gameState.CameraMode == 0) {
        glutPassiveMotionFunc(passiveMouseMotionCallback);
        glutWarpPointer(gameState.windowWidth/2, gameState.windowHeight/2);
      }
      else {
        glutPassiveMotionFunc(NULL);
      }
      break;
    case'e': { // insert explosion randomly
        const glm::vec3 explosionPosition = glm::vec3(
          (float)(2.0 * (rand() / (double)RAND_MAX) - 1.0),
          (float)(2.0 * (rand() / (double)RAND_MAX) - 1.0),
          0.0f
        );
        insertExplosion(explosionPosition);
      }
      break;
    default:
      ; // printf("Unrecognized key pressed\n");
  }
}

// Called whenever a key on the keyboard was released. The key is given by
// the "keyReleased" parameter, which is in ASCII. 
void keyboardUpCallback(unsigned char keyReleased, int mouseX, int mouseY) {

  switch(keyReleased) {
    case ' ':
      gameState.keyMap[KEY_SPACE] = false;
      break;
    default:
      ; // printf("Unrecognized key released\n");
  }
}

// The special keyboard callback is triggered when keyboard function or directional
// keys are pressed.
void specialKeyboardCallback(int specKeyPressed, int mouseX, int mouseY) {

  if(gameState.gameOver == true)
    return;

  switch (specKeyPressed) {
    case GLUT_KEY_RIGHT:
      gameState.keyMap[KEY_RIGHT_ARROW] = true;
      break;
    case GLUT_KEY_LEFT:
      gameState.keyMap[KEY_LEFT_ARROW] = true;
      break;
    case GLUT_KEY_UP:
      gameState.keyMap[KEY_UP_ARROW] = true;
      break;
    case GLUT_KEY_DOWN:
      gameState.keyMap[KEY_DOWN_ARROW] = true;
      break;
    default:
      ; // printf("Unrecognized special key pressed\n");
  }
}

// The special keyboard callback is triggered when keyboard function or directional
// keys are released.
void specialKeyboardUpCallback(int specKeyReleased, int mouseX, int mouseY) {

  if(gameState.gameOver == true)
    return;

  switch (specKeyReleased) {
	case GLUT_KEY_RIGHT:
      gameState.keyMap[KEY_RIGHT_ARROW] = false;
      break;
    case GLUT_KEY_LEFT:
      gameState.keyMap[KEY_LEFT_ARROW] = false;
      break;
    case GLUT_KEY_UP:
      gameState.keyMap[KEY_UP_ARROW] = false;
      break;
    case GLUT_KEY_DOWN:
      gameState.keyMap[KEY_DOWN_ARROW] = false;
      break;
    default:
      ; // printf("Unrecognized special key released\n");
  }
}

void initMeshes() {

    gameObjects.Grid = new StaticMesh();
    gameObjects.Grid->geometry = createSquareGrid(200, glm::vec3(0.0f, 1.0f, 0.0f), 0.2f);
    gameObjects.Grid->isActive = true;
    gameObjects.Grid->position = glm::vec3(0.0f, 0.0f, 0.0f);
    gameObjects.Grid->scale = glm::vec3(1.f);
    gameObjects.Grid->rotation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));

    // Gasloina
    float angleInDegrees = 90.0f;  // �hel v stupn�ch
    float angleInRadians = glm::radians(angleInDegrees);

    glm::quat rotation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));  // ��dn� rotace
    glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);  // Pozice v centru
    glm::vec3 scale = glm::vec3(0.1f, 0.1f, 0.1f);  // Standardn� m??�tko 1:1

    gameObjects.Barrels = new StaticMeshs * [barrelsCount];

    gameObjects.Barrels[0] = createStaticMeshs(rotation, glm::vec3(-2.0f, 0.01f, 3.4f), glm::vec3(0.007f, 0.007f, 0.007f), BARREL, shaderProgram);
    gameObjects.Barrels[1] = createStaticMeshs(rotation, glm::vec3(-0.3f, 0.01f, 3.2f), glm::vec3(0.007f, 0.007f, 0.007f), BARREL, shaderProgram);
    gameObjects.Barrels[2] = createStaticMeshs(rotation, glm::vec3(-0.9f, 0.01f, 3.65f), glm::vec3(0.007f, 0.007f, 0.007f), BARREL, shaderProgram);

    gameObjects.Roads = new StaticMeshs * [roadBlocksCount];

    for (int i = 0; i < roadBlocksCount; ++i) {
        float rx = roadBlocksProperties[i * 6 + 0];
        float ry = roadBlocksProperties[i * 6 + 1];
        float rz = roadBlocksProperties[i * 6 + 2];
        float px = roadBlocksProperties[i * 6 + 3];
        float py = roadBlocksProperties[i * 6 + 4];
        float pz = roadBlocksProperties[i * 6 + 5];

        glm::quat rotation = glm::quat(glm::vec3(rx, ry, rz));
        glm::vec3 position = glm::vec3(px, py, pz);
        glm::vec3 scale = roadBlocksScale;

        gameObjects.Roads[i] = createStaticMeshs(rotation, position, scale, ROAD, shaderProgram);
    }

    position = glm::vec3(1.0f, 0.0f, 0.0f);

    gameObjects.F1 = createStaticMeshs(rotation, glm::vec3(1.0f, -0.01f, 1.0f), glm::vec3(0.02f, 0.02f, 0.02f), CAR, shaderProgram);


    // Definice um�st�n�, rotace a m���tka pro model auta
    glm::vec3 carPosition = glm::vec3(0.0f, 0.002f, 1.0f);
    glm::quat carDirection = glm::quat(glm::vec3(0.0f, 1.0f, 0.0f));
    glm::vec3 carScale = glm::vec3(0.001f, 0.001f, 0.001f);

    // Vytvo�en� modelu auta
    gameObjects.F1 = createStaticMeshs(carDirection, carPosition, carScale, F1, shaderProgram);
    gameObjects.F1->startPosition = gameObjects.F1->position;
    gameObjects.F1->playAnimation = false;
    gameState.F1DeltaTime = 0;

    gameObjects.Car1 = createStaticMeshs(rotation, glm::vec3(0.5f, -0.01f, 0.5f), glm::vec3(0.02f, 0.02f, 0.02f), CAR, shaderProgram);
    gameObjects.Car2 = createStaticMeshs(rotation, glm::vec3(0.5f, 0.05f, 1.5f), glm::vec3(0.04f, 0.04f, 0.04f), CHEVROLET, shaderProgram);

    for (int i = 430; i <= 480; i++){ //626
        gameObjects.Car1->geometry[i]->alpha = 0.8f;
    }
    std::cout << "Objects " << gameObjects.Car1->numMeshes << " mucho." << std::endl;

    gameObjects.Lamps = new StaticMeshs * [3];
    gameObjects.Lamps[0] = createStaticMeshs(rotation, glm::vec3(0.3f, 0.0f, 0.5f), glm::vec3(0.02f, 0.02f, 0.02f), LAMP, shaderProgram);
    gameObjects.Lamps[1] = createStaticMeshs(rotation, glm::vec3(0.3f, 0.0f, 1.5f), glm::vec3(0.02f, 0.02f, 0.02f), LAMP, shaderProgram);
    gameObjects.Lamps[2] = createStaticMeshs(rotation, glm::vec3(0.3f, 0.0f, -0.5f), glm::vec3(0.02f, 0.02f, 0.02f), LAMP, shaderProgram);

    gameObjects.Houses = new StaticMeshs * [2];
    gameObjects.Houses[0] = createStaticMeshs(glm::quat(glm::vec3(0.0f, angleInRadians, 0.0f)), glm::vec3(1.5f, -0.01f, 0.5f), glm::vec3(0.07f, 0.07f, 0.07f), House, shaderProgram);
    gameObjects.Houses[1] = createStaticMeshs(glm::quat(glm::vec3(0.0f, angleInRadians, 0.0f)), glm::vec3(1.5f, -0.01f, 1.5f), glm::vec3(0.07f, 0.07f, 0.07f), House, shaderProgram);
    
    gameObjects.Houses[0]->geometry[16]->alpha = 0.6f;
    gameObjects.Houses[1]->geometry[16]->alpha = 0.6f;


    gameObjects.Anvil = createStaticMesh(glm::quat(glm::vec3(0.0f, angleInRadians, 0.0f)), glm::vec3(0.7f, 0.05f, 1.8f), glm::vec3(0.07f, 0.07f, 0.07f), Anvil);

    gameObjects.Rocks = new StaticMeshs * [5];

    gameObjects.Rocks[0] = createStaticMeshs(glm::quat(glm::vec3(0.0f, angleInRadians, 0.0f)), glm::vec3(-1.0f, -0.001f, -0.5f), glm::vec3(0.1f, 0.1f, 0.1f), ROCK, shaderProgram);
    gameObjects.Rocks[1] = createStaticMeshs(glm::quat(glm::vec3(0.0f, angleInRadians, 0.0f)), glm::vec3(-1.5f, -0.001f, -0.8f), glm::vec3(0.1f, 0.1f, 0.1f), ROCK, shaderProgram);
    gameObjects.Rocks[2] = createStaticMeshs(rotation, glm::vec3(-1.2f, -0.001f, -1.2f), glm::vec3(0.1f, 0.1f, 0.1f), ROCK, shaderProgram);
    gameObjects.Rocks[3] = createStaticMeshs(rotation, glm::vec3(-1.6f, -0.001f, -0.6f), glm::vec3(0.1f, 0.1f, 0.1f), ROCK, shaderProgram);
    gameObjects.Rocks[4] = createStaticMeshs(glm::quat(glm::vec3(0.0f, angleInRadians, 0.0f)), glm::vec3(-1.1f, -0.001f, -0.9f), glm::vec3(0.1f, 0.1f, 0.1f), ROCK, shaderProgram);

    gameObjects.HardCodedRock = new StaticMesh;
    initializeHardCodedMesh(gameObjects.HardCodedRock, shaderProgram);
    setAlpha(0.5f);
}

// Called after the window and OpenGL are initialized. Called exactly once, before the main loop.
void initializeApplication() {

  glutMouseFunc(onMouseClickCallback);

  // initialize random seed
  srand ((unsigned int)time(NULL));

  // initialize OpenGL
  glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
  glEnable(GL_DEPTH_TEST);

  useLighting = true;

  // initialize shaders
  initializeShaderPrograms();
  // create geometry for all models used
  initializeModels();

  gameObjects.bannerObject = NULL;

  // test whether the curve segment is correctly computed (tasks 1 and 2)
  testCurve(evaluateCurveSegment, evaluateCurveSegment_1stDerivative);

  restartGame();
}

void finalizeApplication(void) {

  cleanUpObjects();

  // delete buffers - space ship, asteroid, missile, ufo, banner, and explosion
  cleanupModels();

  // delete shaders
  cleanupShaderPrograms();
}

int main(int argc, char** argv) {

  // initialize windowing system
  glutInit(&argc, argv);

#ifndef __APPLE__
  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
#else
  glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
#endif

  // initial camera
  gameState.cameraPosition = glm::vec3(0.0f, 0.0f, 2.0f);  // Example position
  gameState.cameraDirection = glm::vec3(0.0f, 0.0f, 1.0f); // Looking forward
  gameState.cameraUpVector = glm::vec3(0.0f, 1.0f, 0.0f);
  gameState.cameraYaw = 180.0f;    // Facing straight forward
  gameState.cameraPitch = 0.0f;  // Level with horizon
  gameState.cameraSpeed = 0.1f;

  // initial window size
  glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
  glutCreateWindow(WINDOW_TITLE);

  glutDisplayFunc(displayCallback);
  // register callback for change of window size
  glutReshapeFunc(reshapeCallback);
  // register callbacks for keyboard
  glutKeyboardFunc(keyboardCallback);
  glutKeyboardUpFunc(keyboardUpCallback);
  glutSpecialFunc(specialKeyboardCallback);     // key pressed
  glutSpecialUpFunc(specialKeyboardUpCallback); // key released


  glutTimerFunc(33, timerCallback, 0);

  // initialize PGR framework (GL, DevIl, etc.)
  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  initializeApplication();

#ifndef __APPLE__
  glutCloseFunc(finalizeApplication);
#else
  glutWMCloseFunc(finalizeApplication);
#endif

  initMeshes();

  glutMainLoop();

  return 0;
}
