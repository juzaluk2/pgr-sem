#version 140

struct Material {
  vec3  ambient;
  vec3  diffuse;
  vec3  specular;
  float shininess;
  bool  useTexture;
};

uniform Material material;
uniform sampler2D texSampler;

in vec2 fragTexCoord;

out vec4 color;

void main() {
  // Pro jednoduchost, vrátíme jen difuzní barvu materiálu.
  vec3 baseColor = material.diffuse;

  // Pokud je textura povolena, aplikujeme ji
  if (material.useTexture) {
    baseColor *= texture(texSampler, fragTexCoord).rgb;
  }

  color = vec4(baseColor, 1.0);
}
