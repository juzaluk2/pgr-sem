//----------------------------------------------------------------------------------------
/**
 * \file    asteroids.cpp
 * \author  Jaroslav Sloup, Tomas Barak, Petr Felkel
 * \date    2011-2012
 * \brief   Simple implementaion of Asteroids game.
 */
//----------------------------------------------------------------------------------------

#include <time.h>
#include <list>
#include "pgr.h"
#include "render_stuff.h"
#include "spline.h"
#include <iostream>


extern SCommonShaderProgram shaderProgram;
extern bool useLighting;

typedef std::list<void *> GameObjectsList; 

const char* GASOLINE = "data/GasStation/10072_Gas Station_V4_L3.obj";
const char* CAR = "data/Car/car.obj";
const char* CHEVROLET = "data/Chevrolet/Chevrolet_Camaro_SS_High.obj";
const char* ROAD = "data/Road/old road.obj";
const char* COTTAGE = "data/Cottage/Cottage_free.obj";
const char* BARREL = "data/Barrel/Barrel.obj";

struct GameState {

  int windowWidth;    // set by reshape callback
  int windowHeight;   // set by reshape callback

  int CameraMode;        // false;
  float cameraElevationAngle; // in degrees = initially 0.0f

  // New camera variables
  glm::vec3 cameraPosition;   // Add default position
  glm::vec3 cameraDirection;  // Add default direction
  glm::vec3 cameraUpVector;   // Usually glm::vec3(0.0f, 0.0f, 1.0f)

  float cameraYaw;   // Horizontal angle
  float cameraPitch; // Vertical angle
  float cameraSpeed;

  bool gameOver;              // false;
  bool keyMap[KEYS_COUNT];    // false

  float elapsedTime;

} gameState;

struct GameObjects {

  StaticMesh *Anvil;
  StaticMesh *Grid;
  StaticMeshs* Car;
  StaticMeshs** Barrels;
  StaticMeshs** Roads;

  StaticMeshs* Cottage;

  GameObjectsList explosions;
  BannerObject* bannerObject; // NULL;
} gameObjects;

void updateCameraDirection();

void insertExplosion(const glm::vec3 &position) {

  ExplosionObject* newExplosion = new ExplosionObject;

  newExplosion->speed = 0.0f;
  newExplosion->destroyed = false;

  newExplosion->startTime = gameState.elapsedTime;
  newExplosion->currentTime = newExplosion->startTime;

  newExplosion->size = BILLBOARD_SIZE;
  newExplosion->direction = glm::vec3(0.0f, 0.0f, 1.0f);

  newExplosion->frameDuration = 0.1f;
  newExplosion->textureFrames = 16;

  newExplosion->position = position;

  gameObjects.explosions.push_back(newExplosion);
}

void teleport(void) {
}

void cleanUpObjects(void) {

  // delete explosions
  while(!gameObjects.explosions.empty()) {
    delete gameObjects.explosions.back();
    gameObjects.explosions.pop_back();
  } 

  // remove banner
  if(gameObjects.bannerObject != NULL) {
    delete gameObjects.bannerObject;
    gameObjects.bannerObject = NULL;
  }
}

// generates random position that does not collide with the spaceship
glm::vec3 generateRandomPosition(void) {
 glm::vec3 newPosition;

    // position is generated randomly
    // coordinates are in range -1.0f ... 1.0f
    newPosition = glm::vec3(
      (float)(2.0 * (rand() / (double)RAND_MAX) - 1.0),
      (float)(2.0 * (rand() / (double)RAND_MAX) - 1.0),
      0.0f
    );

  return newPosition;
}

StaticMesh* createStaticMesh(const glm::quat& rotation, const glm::vec3& position, const glm::vec3& scale, const char* path) {
    StaticMesh* staticMesh = new StaticMesh;  // Vytvo?en� nov� instance StaticMesh

    // Nastaven� transforma?n�ch atribut?
    staticMesh->position = position;
    staticMesh->rotation = rotation;
    staticMesh->scale = scale;
    staticMesh->isActive = true; // Nastaven� mesh jako aktivn�, m?�ete p?idat parametr funkce pro toto nastaven�

    // Na?ten� geometrie pro mesh
    if (!loadSingleMesh(path, shaderProgram, &staticMesh->geometry)) {
        std::cerr << "initializeModels(): model loading failed." << std::endl;
        delete staticMesh; // Uvoln?n� pam?ti, pokud na?ten� sel�e
        return nullptr;
    }

    return staticMesh; // Vr�t� inicializovan� objekt StaticMesh
}

StaticMeshs* createStaticMeshs(const glm::quat& rotation, const glm::vec3& position, const glm::vec3& scale, const char* path, SCommonShaderProgram& shaderProgram) {
    StaticMeshs* staticMesh = new StaticMeshs;  // Vytvo�en� nov� instance StaticMeshs

    // Nastaven� transforma�n�ch atribut�
    staticMesh->position = position;
    staticMesh->rotation = rotation;
    staticMesh->scale = scale;
    staticMesh->isActive = true; // Nastaven� mesh jako aktivn�, m��ete p�idat parametr funkce pro toto nastaven�

    // Na�ten� geometrie pro mesh
    if (!loadGeometry(path, shaderProgram, staticMesh)) {
        std::cerr << "Initialization failed: Model loading failed." << std::endl;
        delete staticMesh; // Uvoln�n� pam�ti, pokud na�ten� sel�e
        return nullptr;
    }

    return staticMesh; // Vr�t� inicializovan� objekt StaticMeshs
}

void restartGame(void) {

  cleanUpObjects();

  gameState.elapsedTime = 0.001f * (float)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds
  
  gameState.CameraMode = 0;
  gameState.cameraElevationAngle = 0.0f;

  // reset key map
  for(int i=0; i<KEYS_COUNT; i++)
    gameState.keyMap[i] = false;

  gameState.gameOver = false;
}

BannerObject* createBanner(void) {
 BannerObject* newBanner = new BannerObject;
 
  newBanner->size = BANNER_SIZE;
  newBanner->position = glm::vec3(0.0f, 0.0f, 0.0f);
  newBanner->direction = glm::vec3(0.0f, 1.0f, 0.0f);
  newBanner->speed = 0.0f;
  newBanner->size = 1.0f;

  newBanner->destroyed = false;

  newBanner->startTime = gameState.elapsedTime;
  newBanner->currentTime = newBanner->startTime;

  return newBanner;
}

void drawWindowContents() {

  // setup parallel projection
  const glm::mat4 orthoProjectionMatrix = glm::ortho(
    -SCENE_WIDTH, SCENE_WIDTH,
    -SCENE_HEIGHT, SCENE_HEIGHT,
    -10.0f*SCENE_DEPTH, 10.0f*SCENE_DEPTH
  );
  // static viewpoint - top view
  const glm::mat4 orthoViewMatrix = glm::lookAt(
    glm::vec3(0.0f, 0.0f, 1.0f),
    glm::vec3(0.0f, 0.0f, 0.0f),
    glm::vec3(0.0f, 1.0f, 0.0f)
  );

  // setup camera & projection transform
  glm::mat4 viewMatrix  = orthoViewMatrix;
  glm::mat4 projectionMatrix = orthoProjectionMatrix;

  if(gameState.CameraMode == 0) {

      glm::vec3 cameraCenter = gameState.cameraPosition + gameState.cameraDirection;

      viewMatrix = glm::lookAt(
          gameState.cameraPosition,
          cameraCenter,
          gameState.cameraUpVector
      );

      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth / (float)gameState.windowHeight, 0.1f, 10.0f);
  }
  else if (gameState.CameraMode == 1){
      glm::vec3 offset = glm::vec3(0.0f, 0.5f, -0.8f);
      offset = gameObjects.Car->rotation * offset;
      gameState.cameraPosition = gameObjects.Car->position + offset;
      glm::vec3 cameraTarget = gameObjects.Car->position + glm::vec3(0.0f, -0.1f, 0.0f);
      gameState.cameraDirection = glm::normalize(cameraTarget - gameState.cameraPosition);
      glm::vec3 cameraCenter = gameState.cameraPosition + gameState.cameraDirection;

      gameState.cameraYaw = glm::degrees(atan2(gameState.cameraDirection.z, gameState.cameraDirection.x));
      gameState.cameraPitch = glm::degrees(asin(gameState.cameraDirection.y));

      viewMatrix = glm::lookAt(
          gameState.cameraPosition,
          cameraCenter,
          gameState.cameraUpVector
      );

      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth / (float)gameState.windowHeight, 0.1f, 100.0f);
  }
  else if (gameState.CameraMode == 2){
      gameState.cameraPosition = glm::vec3(3.0f, 1.0f, -1.0f);
      glm::vec3 cameraTarget = gameObjects.Car->position + glm::vec3(0.0f, -0.1f, 0.0f);
      gameState.cameraDirection = glm::normalize(cameraTarget - gameState.cameraPosition);
      glm::vec3 cameraCenter = gameState.cameraPosition + gameState.cameraDirection;

      gameState.cameraYaw = glm::degrees(atan2(gameState.cameraDirection.z, gameState.cameraDirection.x));
      gameState.cameraPitch = glm::degrees(asin(gameState.cameraDirection.y));

      viewMatrix = glm::lookAt(
          gameState.cameraPosition,
          cameraCenter,
          gameState.cameraUpVector
      );

      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth / (float)gameState.windowHeight, 0.1f, 100.0f);

  } else if (gameState.CameraMode == 3){
      gameState.cameraPosition = glm::vec3(-3.0f, 1.0f, -1.0f);
      glm::vec3 cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
      gameState.cameraDirection = glm::normalize(cameraTarget - gameState.cameraPosition);
      glm::vec3 cameraCenter = gameState.cameraPosition + gameState.cameraDirection;

      gameState.cameraYaw = glm::degrees(atan2(gameState.cameraDirection.z, gameState.cameraDirection.x));
      gameState.cameraPitch = glm::degrees(asin(gameState.cameraDirection.y));

      viewMatrix = glm::lookAt(
          gameState.cameraPosition,
          cameraCenter,
          gameState.cameraUpVector
      );
      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth / (float)gameState.windowHeight, 0.1f, 100.0f);
  } else if (gameState.CameraMode == 4){
      glm::vec3 cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
      gameState.cameraDirection = glm::normalize(cameraTarget - gameState.cameraPosition);
      glm::vec3 cameraCenter = gameState.cameraPosition + gameState.cameraDirection;

      gameState.cameraYaw = glm::degrees(atan2(gameState.cameraDirection.z, gameState.cameraDirection.x));
      gameState.cameraPitch = glm::degrees(asin(gameState.cameraDirection.y));

      viewMatrix = glm::lookAt(
          gameState.cameraPosition,
          cameraCenter,
          gameState.cameraUpVector
      );
      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth / (float)gameState.windowHeight, 0.1f, 100.0f);
  }

  glUseProgram(shaderProgram.program);
  glUniform1f(shaderProgram.timeLocation, gameState.elapsedTime);

  glUseProgram(0);


 // draw grid

  drawStaticMesh(gameObjects.Grid, viewMatrix, projectionMatrix);


  // draw Road
 drawStaticMeshs(gameObjects.Cottage, viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);

 for (int i = 0; i < roadBlocksCount; ++i) {
     drawStaticMeshs(gameObjects.Roads[i], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
 }
 
  // draw car
  drawStaticMeshs(gameObjects.Car, viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);

  //draw Barrel
  drawStaticMeshs(gameObjects.Barrels[0], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  drawStaticMeshs(gameObjects.Barrels[1], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);
  drawStaticMeshs(gameObjects.Barrels[2], viewMatrix, projectionMatrix, gameState.cameraPosition, gameState.cameraDirection);

  // draw skybox
  drawSkybox(viewMatrix, projectionMatrix); 

  // draw explosions with depth test disabled
  glDisable(GL_DEPTH_TEST);

  for(GameObjectsList::const_iterator it = gameObjects.explosions.cbegin(); it != gameObjects.explosions.cend(); ++it) {
    ExplosionObject* explosion = (ExplosionObject *)(*it);
    drawExplosion(explosion, viewMatrix, projectionMatrix); 
  }
  glEnable(GL_DEPTH_TEST);

  if(gameState.gameOver == true) {
    // draw game over banner
    if(gameObjects.bannerObject != NULL)
      drawBanner(gameObjects.bannerObject, orthoViewMatrix, orthoProjectionMatrix);
  }
}

// Called to update the display. You should call glutSwapBuffers after all of your
// rendering to display what you rendered.
void displayCallback() {
  GLbitfield mask = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;

  glClear(mask);

  drawWindowContents();

  glutSwapBuffers();
}

// Called whenever the window is resized. The new window size is given, in pixels.
// This is an opportunity to call glViewport or glScissor to keep up with the change in size.
void reshapeCallback(int newWidth, int newHeight) {

  gameState.windowWidth = newWidth;
  gameState.windowHeight = newHeight;

  glViewport(0, 0, (GLsizei) newWidth, (GLsizei) newHeight);
}

void updateCarWheelsRotation() {
    // P?edpokl�d�me, �e p?edn� kola jsou na indexech 14, 15, 16 a zadn� kola na 13, 12, 11
    const int frontWheelsIndices[] = { 14, 15, 16 };
    const int rearWheelsIndices[] = { 13, 12, 11 };
    const int numWheelsPerGroup = 3; // Po?et kol v ka�d� skupin?

    // Vypo?�t�n� �hlu rotace kol na z�klad? rychlosti a ?asu
    float wheelRotationAngle = 5.0f; // Konstanta 5.0f pro zv��en� �?inku

    // Nastaven� offset? a rotace pro p?edn� a zadn� kola
    for (int i = 0; i < numWheelsPerGroup; ++i) {
        int frontIndex = frontWheelsIndices[i];
        int rearIndex = rearWheelsIndices[i];

        // P?edn� kola
        if (gameObjects.Car->geometry[frontIndex] && false) {
            gameObjects.Car->geometry[frontIndex]->componentRotation = glm::angleAxis(wheelRotationAngle, glm::vec3(1, 0, 0)); // Rotace kolem osy x
            gameObjects.Car->geometry[frontIndex]->componentOffset = glm::vec3(1.0f, 0.0f, 0.0f); // Nap?�klad p?edpokl�dan� offset
        }

        // Zadn� kola
        if (gameObjects.Car->geometry[rearIndex] && false) {
            gameObjects.Car->geometry[rearIndex]->componentRotation = glm::angleAxis(wheelRotationAngle, glm::vec3(1, 0, 0)); // Rotace kolem osy x
            gameObjects.Car->geometry[rearIndex]->componentOffset = glm::vec3(-0.5f, 0.0f, 0.0f); // Nap?�klad p?edpokl�dan� offset
        }
    }
}

void updateObjects(float elapsedTime) {
    if (true) {
        gameObjects.Car->position = gameObjects.Car->startPosition + evaluateClosedCurve(curveData, curveSize, elapsedTime);

        glm::vec3 derivative = evaluateClosedCurve_1stDerivative(curveData, curveSize, elapsedTime);
        derivative = glm::normalize(derivative);

        float angleY = atan2(derivative.x, derivative.z);

        glm::quat rotation = glm::angleAxis(angleY, glm::vec3(0.0f, 1.0f, 0.0f));

        gameObjects.Car->rotation = rotation;
    }
    if (gameState.CameraMode == 4){
      gameState.cameraPosition = evaluateClosedCurve(cameraCurveData, cameraCurveSize, elapsedTime/3);
    }
    updateCarWheelsRotation();
}

// Callback responsible for the scene update
void timerCallback(int) {

  // update scene time
  gameState.elapsedTime = 0.001f * (float)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds

  // call appropriate actions according to the currently pressed keys in key map
  // (combinations of keys are supported but not used in this implementation)
  float cameraSpeed = 0.05f; // Adjust camera speed to your liking
  
  if (gameState.CameraMode == 0)
  {
      if (gameState.keyMap[KEY_UP_ARROW] == true) {
          gameState.cameraPosition += cameraSpeed * gameState.cameraDirection;
      }

      if (gameState.keyMap[KEY_DOWN_ARROW] == true) {
          gameState.cameraPosition -= cameraSpeed * gameState.cameraDirection;
      }

      if (gameState.keyMap[KEY_LEFT_ARROW] == true) {
          glm::vec3 cameraRight = glm::normalize(glm::cross(gameState.cameraDirection, gameState.cameraUpVector));
          gameState.cameraPosition -= cameraRight * cameraSpeed;
      }

      if (gameState.keyMap[KEY_RIGHT_ARROW] == true) {
          glm::vec3 cameraRight = glm::normalize(glm::cross(gameState.cameraDirection, gameState.cameraUpVector));
          gameState.cameraPosition += cameraRight * cameraSpeed;
      }
  }

  if((gameState.gameOver == true) && (gameObjects.bannerObject != NULL)) {
    gameObjects.bannerObject->currentTime = gameState.elapsedTime;
  }

  // update objects in the scene
  updateObjects(gameState.elapsedTime);

  // space pressed -> launch missile
  if(gameState.keyMap[KEY_SPACE] == true) {

  }

  // game over? -> create banner with scrolling text "game over"
  if(gameState.gameOver == true) {
    gameState.keyMap[KEY_SPACE] = false;
    if(gameObjects.bannerObject == NULL) {
      // if game over and banner still not created -> create banner
      gameObjects.bannerObject = createBanner();
    }
  }

  // set timeCallback next invocation
  glutTimerFunc(33, timerCallback, 0);

  glutPostRedisplay();
}

void updateCameraDirection() {
    // Convert spherical to Cartesian coordinates to update camera direction
    glm::vec3 direction;
    direction.x = cos(glm::radians(gameState.cameraYaw)) * cos(glm::radians(gameState.cameraPitch));
    direction.y = sin(glm::radians(gameState.cameraPitch));
    direction.z = sin(glm::radians(gameState.cameraYaw)) * cos(glm::radians(gameState.cameraPitch));
    gameState.cameraDirection = glm::normalize(direction);
}

// Called when mouse is moving while no mouse buttons are pressed.
void passiveMouseMotionCallback(int mouseX, int mouseY) {

    if (gameState.CameraMode == 0) {
        // Calculate the new angles based on mouse position
        float deltaX = 0.3f * (mouseX - gameState.windowWidth / 2);   // Horizontal change
        float deltaY = 0.3f * (mouseY - gameState.windowHeight / 2);  // Vertical change

        gameState.cameraYaw += deltaX;
        gameState.cameraPitch -= deltaY;

        // Clamp the camera pitch to prevent the camera from flipping over
        gameState.cameraPitch = glm::clamp(gameState.cameraPitch, -85.0f, 85.0f);

        // Update camera direction
        updateCameraDirection();

        // Set the mouse pointer back to the window center to avoid drifting
        glutWarpPointer(gameState.windowWidth / 2, gameState.windowHeight / 2);
    }
}

// Called whenever a key on the keyboard was pressed. The key is given by the "keyPressed"
// parameter, which is in ASCII. It's often a good idea to have the escape key (ASCII value 27)
// to call glutLeaveMainLoop() to exit the program.
void keyboardCallback(unsigned char keyPressed, int mouseX, int mouseY) {
  
  switch(keyPressed) {
    case 27: // escape
#ifndef __APPLE__
        glutLeaveMainLoop();
#else
        exit(0);
#endif
      break;
    case 'r': // restart game
      restartGame();
      break;
    case ' ': // launch missile
      if(gameState.gameOver != true)
        gameState.keyMap[KEY_SPACE] = true;
      break;
    case 't': // teleport space ship
      if(gameState.gameOver != true)
        teleport();
      break;
    case 'c': // switch camera
      gameState.CameraMode += 1;
      gameState.CameraMode = gameState.CameraMode % 5;
      if(gameState.CameraMode == 0) {
        glutPassiveMotionFunc(passiveMouseMotionCallback);
        glutWarpPointer(gameState.windowWidth/2, gameState.windowHeight/2);
      }
      else {
        glutPassiveMotionFunc(NULL);
      }
      break;
    case'e': { // insert explosion randomly
        const glm::vec3 explosionPosition = glm::vec3(
          (float)(2.0 * (rand() / (double)RAND_MAX) - 1.0),
          (float)(2.0 * (rand() / (double)RAND_MAX) - 1.0),
          0.0f
        );
        insertExplosion(explosionPosition);
      }
      break;
    default:
      ; // printf("Unrecognized key pressed\n");
  }
}

// Called whenever a key on the keyboard was released. The key is given by
// the "keyReleased" parameter, which is in ASCII. 
void keyboardUpCallback(unsigned char keyReleased, int mouseX, int mouseY) {

  switch(keyReleased) {
    case ' ':
      gameState.keyMap[KEY_SPACE] = false;
      break;
    default:
      ; // printf("Unrecognized key released\n");
  }
}

// The special keyboard callback is triggered when keyboard function or directional
// keys are pressed.
void specialKeyboardCallback(int specKeyPressed, int mouseX, int mouseY) {

  if(gameState.gameOver == true)
    return;

  switch (specKeyPressed) {
    case GLUT_KEY_RIGHT:
      gameState.keyMap[KEY_RIGHT_ARROW] = true;
      break;
    case GLUT_KEY_LEFT:
      gameState.keyMap[KEY_LEFT_ARROW] = true;
      break;
    case GLUT_KEY_UP:
      gameState.keyMap[KEY_UP_ARROW] = true;
      break;
    case GLUT_KEY_DOWN:
      gameState.keyMap[KEY_DOWN_ARROW] = true;
      break;
    default:
      ; // printf("Unrecognized special key pressed\n");
  }
}

// The special keyboard callback is triggered when keyboard function or directional
// keys are released.
void specialKeyboardUpCallback(int specKeyReleased, int mouseX, int mouseY) {

  if(gameState.gameOver == true)
    return;

  switch (specKeyReleased) {
	case GLUT_KEY_RIGHT:
      gameState.keyMap[KEY_RIGHT_ARROW] = false;
      break;
    case GLUT_KEY_LEFT:
      gameState.keyMap[KEY_LEFT_ARROW] = false;
      break;
    case GLUT_KEY_UP:
      gameState.keyMap[KEY_UP_ARROW] = false;
      break;
    case GLUT_KEY_DOWN:
      gameState.keyMap[KEY_DOWN_ARROW] = false;
      break;
    default:
      ; // printf("Unrecognized special key released\n");
  }
}

void initMeshes() {

    gameObjects.Grid = new StaticMesh();
    gameObjects.Grid->geometry = createSquareGrid(100, glm::vec3(0.0f, 1.0f, 0.0f), 0.1f);
    gameObjects.Grid->isActive = true;
    gameObjects.Grid->position = glm::vec3(0.0f, 0.0f, 0.0f);
    gameObjects.Grid->scale = glm::vec3(1.f);
    gameObjects.Grid->rotation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));

    // Gasloina
    float angleInDegrees = 90.0f;  // �hel v stupn�ch
    float angleInRadians = glm::radians(angleInDegrees);

    glm::quat rotation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));  // ��dn� rotace
    glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);  // Pozice v centru
    glm::vec3 scale = glm::vec3(0.1f, 0.1f, 0.1f);  // Standardn� m??�tko 1:1

    gameObjects.Barrels = new StaticMeshs * [barrelsCount];

    gameObjects.Barrels[0] = createStaticMeshs(rotation, glm::vec3(-2.0f, 0.01f, 3.4f), glm::vec3(0.007f, 0.007f, 0.007f), BARREL, shaderProgram);
    gameObjects.Barrels[1] = createStaticMeshs(rotation, glm::vec3(-0.3f, 0.01f, 3.2f), glm::vec3(0.007f, 0.007f, 0.007f), BARREL, shaderProgram);
    gameObjects.Barrels[2] = createStaticMeshs(rotation, glm::vec3(-0.9f, 0.01f, 3.65f), glm::vec3(0.007f, 0.007f, 0.007f), BARREL, shaderProgram);

    gameObjects.Roads = new StaticMeshs * [roadBlocksCount];

    for (int i = 0; i < roadBlocksCount; ++i) {
        float rx = roadBlocksProperties[i * 6 + 0];
        float ry = roadBlocksProperties[i * 6 + 1];
        float rz = roadBlocksProperties[i * 6 + 2];
        float px = roadBlocksProperties[i * 6 + 3];
        float py = roadBlocksProperties[i * 6 + 4];
        float pz = roadBlocksProperties[i * 6 + 5];

        glm::quat rotation = glm::quat(glm::vec3(rx, ry, rz));
        glm::vec3 position = glm::vec3(px, py, pz);
        glm::vec3 scale = roadBlocksScale;

        gameObjects.Roads[i] = createStaticMeshs(rotation, position, scale, ROAD, shaderProgram);
    }

    position = glm::vec3(1.0f, 0.0f, 0.0f);
    gameObjects.Cottage = createStaticMeshs(rotation, position, scale, COTTAGE, shaderProgram);

    // Definice um�st�n�, rotace a m���tka pro model auta
    glm::vec3 carPosition = glm::vec3(0.0f, 0.1f, 1.0f);
    glm::quat carDirection = glm::quat(glm::vec3(0.0f, 1.0f, 0.0f));
    glm::vec3 carScale = glm::vec3(0.05f, 0.05f, 0.05f);

    // Vytvo�en� modelu auta
    gameObjects.Car = createStaticMeshs(carDirection, carPosition, carScale, CHEVROLET, shaderProgram);
    gameObjects.Car->startPosition = gameObjects.Car->position;
}

// Called after the window and OpenGL are initialized. Called exactly once, before the main loop.
void initializeApplication() {

  // initialize random seed
  srand ((unsigned int)time(NULL));

  // initialize OpenGL
  glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
  glEnable(GL_DEPTH_TEST);

  useLighting = true;

  // initialize shaders
  initializeShaderPrograms();
  // create geometry for all models used
  initializeModels();

  gameObjects.bannerObject = NULL;

  // test whether the curve segment is correctly computed (tasks 1 and 2)
  testCurve(evaluateCurveSegment, evaluateCurveSegment_1stDerivative);

  restartGame();
}

void finalizeApplication(void) {

  cleanUpObjects();

  // delete buffers - space ship, asteroid, missile, ufo, banner, and explosion
  cleanupModels();

  // delete shaders
  cleanupShaderPrograms();
}

int main(int argc, char** argv) {

  // initialize windowing system
  glutInit(&argc, argv);

#ifndef __APPLE__
  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
#else
  glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
#endif

  // initial camera
  gameState.cameraPosition = glm::vec3(0.0f, 0.0f, 2.0f);  // Example position
  gameState.cameraDirection = glm::vec3(0.0f, 0.0f, 1.0f); // Looking forward
  gameState.cameraUpVector = glm::vec3(0.0f, 1.0f, 0.0f);
  gameState.cameraYaw = 180.0f;    // Facing straight forward
  gameState.cameraPitch = 0.0f;  // Level with horizon
  gameState.cameraSpeed = 0.1f;

  // initial window size
  glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
  glutCreateWindow(WINDOW_TITLE);

  glutDisplayFunc(displayCallback);
  // register callback for change of window size
  glutReshapeFunc(reshapeCallback);
  // register callbacks for keyboard
  glutKeyboardFunc(keyboardCallback);
  glutKeyboardUpFunc(keyboardUpCallback);
  glutSpecialFunc(specialKeyboardCallback);     // key pressed
  glutSpecialUpFunc(specialKeyboardUpCallback); // key released


  glutTimerFunc(33, timerCallback, 0);

  // initialize PGR framework (GL, DevIl, etc.)
  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  initializeApplication();

#ifndef __APPLE__
  glutCloseFunc(finalizeApplication);
#else
  glutWMCloseFunc(finalizeApplication);
#endif

  initMeshes();

  glutMainLoop();

  return 0;
}
