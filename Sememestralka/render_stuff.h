//----------------------------------------------------------------------------------------
/**
 * \file    render_stuff.h
 * \author  Jaroslav Sloup, Tomas Barak, Petr Felkel
 * \date    2013
 * \brief   Rendering stuff - drawing functions for models, etc.
 */
//----------------------------------------------------------------------------------------

#ifndef __RENDER_STUFF_H
#define __RENDER_STUFF_H

#include "data.h"

// defines geometry of object in the scene (space ship, ufo, asteroid, etc.)
// geometry is shared among all instances of the same object type
typedef struct _MeshGeometry {
  GLuint        vertexBufferObject;   // identifier for the vertex buffer object
  GLuint        elementBufferObject;  // identifier for the element buffer object
  GLuint        vertexArrayObject;    // identifier for the vertex array object
  unsigned int  numTriangles;         // number of triangles in the mesh
  // material
  glm::vec3     ambient;
  glm::vec3     diffuse;
  glm::vec3     specular;
  float         shininess;
  GLuint        texture;
  float         alpha;

  glm::vec3 componentOffset;
  glm::quat componentRotation;

} MeshGeometry;

typedef struct _StaticMesh {
    MeshGeometry *geometry;

    glm::vec3 position;  
    glm::vec3 scale;
    glm::quat rotation;

    bool isActive;

} StaticMesh;

typedef struct _StaticMeshs {
    MeshGeometry** geometry;
    unsigned int numMeshes;

    glm::vec3 startPosition;
    glm::vec3 position;
    glm::vec3 scale;
    glm::quat rotation;

    bool isActive;
    bool playAnimation;

} StaticMeshs;

// parameters of individual objects in the scene (e.g. position, size, speed, etc.)
typedef struct _Object {
  glm::vec3 position;
  glm::vec3 direction;
  float     speed;
  float     size;

  bool destroyed;

  float startTime;
  float currentTime;

} Object;


typedef struct _ExplosionObject : public Object {

  int    textureFrames;
  float  frameDuration;

} ExplosionObject;

typedef struct _BannerObject : public Object {

} BannerObject;

typedef struct _Vertex {
    glm::vec3 position;
    glm::vec2 texCoords;
    glm::vec3 normal;
} Vertex;

typedef struct _commonShaderProgram {
  // identifier for the shader program
  GLuint program;          // = 0;
  // vertex attributes locations
  GLint posLocation;       // = -1;
  GLint colorLocation;     // = -1;
  GLint normalLocation;    // = -1;
  GLint texCoordLocation;  // = -1;
  // uniforms locations
  GLint PVMmatrixLocation;    // = -1;
  GLint VmatrixLocation;      // = -1;  view/camera matrix
  GLint MmatrixLocation;      // = -1;  modeling matrix
  GLint normalMatrixLocation; // = -1;  inverse transposed Mmatrix

  GLint timeLocation;         // = -1; elapsed time in seconds

  //fog
  GLint fog;

  GLint alpha;

  // material 
  GLint diffuseLocation;    // = -1;
  GLint ambientLocation;    // = -1;
  GLint specularLocation;   // = -1;
  GLint shininessLocation;  // = -1;
  // texture
  GLint useTextureLocation; // = -1; 
  GLint texSamplerLocation; // = -1;
  // reflector related uniforms
  GLint reflectorPositionLocation;  // = -1; 
  GLint reflectorDirectionLocation; // = -1;
  // Light Switches
  GLint lamp1;
  GLint lamp2;
  GLint lamp3;
  // kamera
  GLint cameraLightPositionLocation;
  GLint cameraLightDirectionLocation;
} SCommonShaderProgram;

void drawExplosion(ExplosionObject* explosion, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
void drawBanner(BannerObject* banner, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
void drawSkybox(const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
bool loadSingleMesh(const std::string& fileName, SCommonShaderProgram& shader, MeshGeometry** geometry);
void drawStaticMesh(StaticMesh* mesh, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
MeshGeometry* createSquareGrid(int gridSize, glm::vec3 color, float tileSize);
void drawGrid(StaticMesh* grid, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);
bool loadGeometry(const std::string& fileName, SCommonShaderProgram& shader, StaticMeshs* staticMesh);
void drawStaticMeshs(StaticMeshs* mesh, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, glm::vec3 cameraPosition, glm::vec3 cameraDirection);
void setFog(bool fog);
void setLamps(int lamp, bool isOn);
void initializeHardCodedMesh(StaticMesh* mesh, SCommonShaderProgram& shader);
void setAlpha(float alpha);

void initializeShaderPrograms();
void cleanupShaderPrograms();

void initializeModels();
void cleanupModels();

#endif __RENDER_STUFF_H
